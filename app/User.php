<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract
{
    
    use Authenticatable, CanResetPassword, HasRoleAndPermission;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'user_type', 'address', 'phone_number', 'id_number', 'seedbed_id', 'legal_entity_type', 'legal_entity_number', 'contact_person'];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    public static $rules = [
        'name' => 'required', 
        'email' => 'email|unique:users|required', 
        'password' => 'required',
        'user_type' => 'in:Pengelola,Perorangan,Badan Hukum',
        'address' => 'required',
        'phone_number' => 'required',
    ];
    
    public function getUrlForPaswordReset($token) {
        $email = $this->getEmailForPasswordReset();
        return url('../app/password/reset/'.$token.'?email='.$email);
    }

    public function role_user() {
        return $this->hasOne('App\Models\RoleUser', 'user_id', 'id');
    }
    
    public function roles() {
        return $this->belongsToMany('App\Models\Role', 'role_user', 'user_id');
    }

    // Pengelola kecamatan
    public function districts() {
        return $this->belongsToMany('App\Models\District', 'district_user', 'user_id');
    }
}
