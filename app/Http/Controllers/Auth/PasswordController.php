<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Mitul\Controller\AppBaseController;

use Illuminate\Support\Facades\Password;

use Mitul\Generator\Utils\ResponseManager;
use Illuminate\Http\Request;
use Response;

use Illuminate\Mail\Message;

class PasswordController extends AppBaseController {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		// $this->middleware('guest');
	}

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return Response::json(['status' => trans($response)], 200, [], JSON_NUMERIC_CHECK);

            case Password::INVALID_USER:
                return Response::json(['error_description' => trans($response)], 400, [], JSON_NUMERIC_CHECK);
        }
    }

    /**
     * Overriding
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $this->validateRequest($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return Response::json(['status' => 'Password reset successfully.'], 200, [], JSON_NUMERIC_CHECK);

            case Password::INVALID_USER:
                return Response::json(['error_description' => 'User not found'], 400, [], JSON_NUMERIC_CHECK);

            case Password::INVALID_PASSWORD:
                return Response::json(['error_description' => 'Password invalid'], 400, [], JSON_NUMERIC_CHECK);

            case Password::INVALID_TOKEN:
                return Response::json(['error_description' => 'Token invalid or expired'], 400, [], JSON_NUMERIC_CHECK);

            default:
                return Response::json(['error_description' => 'Unspecified error'], 400, [], JSON_NUMERIC_CHECK);
        }
    }
}
