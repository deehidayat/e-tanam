<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\API\Common\CRUDAPIController;
use Illuminate\Container\Container as Application;
// Ganti Class Repository nya  saja
use App\Libraries\Repositories\PlantingRepository as Repository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Media;

class PlantingAPIController extends CRUDAPIController
{
    
    function __construct(Application $app, Repository $repo) {
        $this->repo = $repo;
        $this->app = $app;
        $this->makeModel();
        
        $this->middleware('oauth_permission');
        $this->beforeFilter('oauth', ['except' => ['index', 'show', 'statistic']]);
    }

    public function store(Request $request) {
        if (sizeof($this->getCreateRules()) > 0) {
            $this->validateRequest($request, $this->getCreateRules());
        }
        $input = $request->all();
        
        $record = $this->repo->create($input);

        /**
         * Script untuk upload file baru dan simpan ke tabel Media
         */
        if (!empty($request->file('photos'))) {
            $photos = [];
            $captions = $request->get('captions');
            foreach ($request->file('photos') as $key => $file) {
                $fileName = 'photos/'.rand().'-'.$request->file('photos')[$key]->getClientOriginalName();
                Storage::put(
                    $fileName,
                    file_get_contents($request->file('photos')[$key]->getRealPath())
                );
                $photos[] = [
                    'url' => 'api/uploads/'. $fileName,
                    'caption' => empty($captions[$key]) ? $request->file('photos')[$key]->getClientOriginalName() : $captions[$key],
                    'owner' => 'plantings',
                    'owner_id' => $record->id
                ];
            }
            Media::insert($photos);
        }

        $meta = array('total' => 1, 'count' => 1, 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record saved successfully.', 'error' => 'Success');
        
        return $this->response($record->toArray(), $meta, 201);
    }

    public function show($id, Request $request) {
        $record = $this->repo->find($id);
        
        if (empty($record)) {
            $this->throwRecordNotFoundException('Record not found', ERROR_CODE_RECORD_NOT_FOUND);
        }
        $meta = array('total' => count($record), 'count' => count($record), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record retrieved successfully.', 'error' => 'Success');
        
        $result = array_merge($record->toArray(), ['photos' => $record->photos->toArray(), 'location' => $record->location->toArray(), 'emission' => $record->emission->toArray()]);

        return $this->response($result, $meta);
    }
    
    public function update($id, Request $request) {
        $record = $this->repo->find($id);
        
        if (empty($record)) {
            $this->throwRecordNotFoundException('Record not found', ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $input = $request->all();
        
        $record = $this->repo->updateRich($input, $id);
        
        if (!$record) {
            $this->throwRecordNotFoundException('Record not saved', ERROR_CODE_VALIDATION_FAILED);
        }
        
        $record = $this->repo->find($id);

        /**
         * Update atau Hapus oldMedia
         */
        $oldMedias = isset($input['oldPhotos']) ? $input['oldPhotos'] : [];
        $deletedMediaId = [];
        $updatedMedia = [];
        foreach ($oldMedias as $key => $media) {
            if (isset($media['deleted']) && $media['deleted']) {
                $deletedMediaId[] = $media['id'];
            } else {
                Media::where('id', $media['id'])->update($media);
            }
        }
        if (count($deletedMediaId)) Media::destroy($deletedMediaId);
        
        /**
         * Script untuk upload file baru dan simpan ke tabel Media
         */
        if (!empty($request->file('photos'))) {
            $photos = [];
            $captions = $request->get('captions');
            foreach ($request->file('photos') as $key => $file) {
                $fileName = 'photos/'.rand().'-'.$request->file('photos')[$key]->getClientOriginalName();
                Storage::put(
                    $fileName,
                    file_get_contents($request->file('photos')[$key]->getRealPath())
                );
                $photos[] = [
                    'url' => 'api/uploads/'. $fileName,
                    'caption' => empty($captions[$key]) ? $request->file('photos')[$key]->getClientOriginalName() : $captions[$key],
                    'owner' => 'plantings',
                    'owner_id' => $record->id
                ];
            }
            Media::insert($photos);
        }
        
        $meta = array('total' => count($record), 'count' => count($record), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record updated successfully.', 'error' => 'Success');
        
        return $this->response($record->toArray(), $meta);
    }

}
