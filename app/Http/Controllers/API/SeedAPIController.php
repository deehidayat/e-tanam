<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\API\Common\CRUDAPIController;
use Illuminate\Container\Container as Application;
// Ganti Class Repository nya  saja
use App\Libraries\Repositories\SeedRepository as Repository;

use Illuminate\Http\Request;

class SeedAPIController extends CRUDAPIController
{
    function __construct(Application $app, Repository $repo) {
        $this->repo = $repo;
        parent::__construct($app);
    }

    public function show($id, Request $request) {
        $record = $this->repo->find($id);
        
        if (empty($record)) {
            $this->throwRecordNotFoundException('Record not found', ERROR_CODE_RECORD_NOT_FOUND);
        }
        $meta = array('total' => count($record), 'count' => count($record), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record retrieved successfully.', 'error' => 'Success');
        
        $result = array_merge($record->toArray(), ['location' => $record->location->toArray()]);

        return $this->response($result, $meta);
    }
}