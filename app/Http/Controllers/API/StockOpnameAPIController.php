<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\API\Common\CRUDAPIController;
use Illuminate\Container\Container as Application;
// Ganti Class Repository nya  saja
use App\Libraries\Repositories\StockOpnameRepository as Repository;

use Illuminate\Http\Request;

use App\Models\SeedbedDetail;
use App\Models\StockOpname;

class StockOpnameAPIController extends CRUDAPIController
{
    function __construct(Application $app, Repository $repo) {
        $this->repo = $repo;
        parent::__construct($app);
    }

    public function index(Request $request) {
        $input = $request->all();
        
        $result = $this->repo->search($input);
        
        $records = $result[0];

        $records = $records->groupBy('number');
        
        $meta = array('total' => $result['total'], 'count' => count($records), 'offset' => isset($input['offset']) ? (int)$input['offset'] : 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Records retrieved successfully.', 'error' => 'Success');
        
        return $this->response($records->toArray(), $meta);
    }
    

    public function store(Request $request) {
        $this->validateRequest($request, [
            'seedbed_id' => 'required',
            'details' => 'array|required'
        ]);
        $input = $request->all();        
        $seedbedDetails = SeedbedDetail::whereIn('id', array_map(function($detail){
            return $detail['id'];
        }, $input['details']))->get();
        $data = [];
        $updated = [];
        $now = \Carbon::now();
        $number = 'TRX-'.$now->format('Ymdhis');
        foreach ($input['details'] as $key => $detail) {
            $prev = $seedbedDetails->find($detail['id']);
            if (SeedbedDetail::find($detail['id'])->update(['quantity' => $detail['new_quantity']])) {
                $data[] = [
                    'number' => $number,
                    'user_id' => 0,
                    'seedbed_id' => $input['seedbed_id'],
                    'seedbed_detail_id' => $detail['id'],
                    'old_quantity' => $prev['quantity'],
                    'new_quantity' => $detail['new_quantity'],
                    'margin' => $detail['new_quantity'] - $prev['quantity'],
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
        }
        if (count($data)) {
            $record = StockOpname::insert($data);
        }

        $meta = array('total' => 1, 'count' => count($data), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record saved successfully.', 'error' => 'Success');        
        return $this->response($data, $meta, 201);
    }   
}