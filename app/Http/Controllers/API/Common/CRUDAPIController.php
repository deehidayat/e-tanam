<?php
namespace App\Http\Controllers\API\Common;

use App\Http\Requests;
use Mitul\Controller\AppBaseController;
use Mitul\Generator\Utils\ResponseManager;
use Illuminate\Http\Request;
use Response;

use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Model;
use Exception;

abstract class CRUDAPIController extends AppBaseController
{
    
    /** 
     * Harus di inisialisasi di __construct
     * @var  Bosnadev\Repositories\Eloquent\Repository 
     */
    protected $repo;

    
    /** 
     * Harus di inisialisasi di __construct
     * @var  Illuminate\Container\Container as Application 
     */
    protected $app;

    /** 
     * Harus di inisialisasi di __construct
     * @var  Illuminate\Database\Eloquent\Model 
     */
    protected $model;
    
    function __construct(Application $app) {
        $this->app = $app;
        $this->makeModel();
        
        $this->middleware('oauth_permission');
        $this->beforeFilter('oauth', ['except' => ['index', 'show']]);
    }
    
    protected function response($data, $meta, $code = 200) {
        return Response::json(ResponseManager::makeResult($data, $meta), $code, [], JSON_NUMERIC_CHECK);
    }

    protected function getCreateRules() {
        return $this->model->rules;
    }

    protected function getUpdateRules($id) {
        return $this->model->rules;
    }

    protected function makeModel()
    {
        $model = $this->app->make($this->repo->model());
        if (!$model instanceof Model) {
            throw new Exception("Class {$this->repo->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }
        return $this->model = $model;
    }

    /**
     * Display a listing of the Record.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request) {
        $input = $request->all();
        
        $result = $this->repo->search($input);
        
        $records = $result[0];
        
        $meta = array('total' => $result['total'], 'count' => count($records), 'offset' => isset($input['offset']) ? (int)$input['offset'] : 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Records retrieved successfully.', 'error' => 'Success');
        
        return $this->response($records->toArray(), $meta);
    }
    
    /**
     * Show the form for creating a new Record.
     *
     * @return Response
     */
    public function create() {
        
        //
        
    }
    
    /**
     * Store a newly created Record in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request) {
        if (sizeof($this->getCreateRules()) > 0) {
            $this->validateRequest($request, $this->getCreateRules());
        }
        $input = $request->all();
        
        $record = $this->repo->create($input);
        
        $meta = array('total' => 1, 'count' => 1, 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record saved successfully.', 'error' => 'Success');
        
        return $this->response($record->toArray(), $meta, 201);
    }
    
    /**
     * Display the specified Record.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request) {
        $record = $this->repo->find($id);
        
        if (empty($record)) {
            $this->throwRecordNotFoundException('Record not found', ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $meta = array('total' => count($record), 'count' => count($record), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record retrieved successfully.', 'error' => 'Success');

        return $this->response($record->toArray(), $meta);
    }
    
    /**
     * Show the form for editing the specified Record.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
        //
        
    }
    
    /**
     * Update the specified Record in storage.
     *
     * @param  int    $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request) {
        $record = $this->repo->find($id);
        
        if (empty($record)) {
            $this->throwRecordNotFoundException('Record not found', ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $input = $request->all();
        
        $record = $this->repo->updateRich($input, $id);
        
        if (!$record) {
            $this->throwRecordNotFoundException('Record not saved', ERROR_CODE_VALIDATION_FAILED);
        }
        
        $record = $this->repo->find($id);
        
        $meta = array('total' => count($record), 'count' => count($record), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record updated successfully.', 'error' => 'Success');
        
        return $this->response($record->toArray(), $meta);
    }
    
    /**
     * Remove the specified Record from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id) {
        $record = $this->repo->find($id);
        
        if (empty($record)) {
            $this->throwRecordNotFoundException('Record not found', ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $record = $this->repo->delete($id);
        
        $meta = array('total' => count($record), 'count' => count($record), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record deleted successfully.', 'error' => 'Success');
        
        return $this->response($id, $meta);
    }
}
