<?php
namespace App\Http\Controllers\API\Common;

use App\Http\Requests;
use Mitul\Controller\AppBaseController;
use Mitul\Generator\Utils\ResponseManager;
use App\User;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Common\UserRepository;
use Response;
use Schema;
use Hash;

class UserAPIController extends AppBaseController
{
    
    /** @var  UserRepository */
    private $userRepository;
    
    function __construct(UserRepository $userRepo) {
        $this->userRepository = $userRepo;
        
        $this->middleware('oauth_permission');
        $this->beforeFilter('oauth', ['except' => ['login', 'signup']]);
    }
    
    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request) {
        $input = $request->all();
        
        $result = $this->userRepository->search($input);
        
        // Ambil Role User
        $userId = \Authorizer::getResourceOwnerId();
        $userLogin = User::find($userId);

        // Filter hanya user yang ada dibawah user login saja
        $users = $result['query']->whereHas('roles', function($q) use($userLogin)
        {
            $q->where('level', '>', $userLogin->level());
        })->get();
        
        $meta = array('total' => $result['total'], 'count' => count($users), 'offset' => isset($input['offset']) ? (int)$input['offset'] : 0, 'last_updated' => $this->userRepository->lastUpdated(), 'status' => "Users retrieved successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($users->toArray(), $meta), 200, [], JSON_NUMERIC_CHECK);
    }
    
    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create() {
        
        //
        
    }
    
    /**
     * Store a newly created User in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request) {
        if (sizeof(User::$rules) > 0) {
            $this->validateRequest($request, User::$rules);
        }
        
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        
        $user = $this->userRepository->create($input);

        if (isset($input['role_id'])) {
            $user->attachRole($input['role_id']);
        }
        
        $meta = array('total' => 1, 'count' => 1, 'offset' => 0, 'last_updated' => $this->userRepository->lastUpdated(), 'status' => "User saved successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($user->toArray(), $meta), 201, [], JSON_NUMERIC_CHECK);
    }
    
    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request) {
        $input = $request->all();
        
        /**
         * Ganti find menggunakan search karena find tidak bisa mengguanakan with???
         */
        
        // $user = $this->userRepository->find($id);
        $result = $this->userRepository->search($input);
        $user = $result[0]->find(intval($id));
        
        if (empty($user)) {
            $this->throwRecordNotFoundException("DisasterEvent not found", ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $meta = array('total' => count($user), 'count' => count($user), 'offset' => 0, 'last_updated' => $this->userRepository->lastUpdated(), 'status' => "DisasterEvent retrieved successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($user->toArray(), $meta), 200, [], JSON_NUMERIC_CHECK);
    }
    
    /**
     * Show the form for editing the specified User.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
        //
        
    }
    
    /**
     * Update the specified User in storage.
     *
     * @param  int    $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request) {
        $user = $this->userRepository->find($id);
        
        if (empty($user)) {
            $this->throwRecordNotFoundException("User not found", ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $input = $request->all();
        if(isset($input['password']) && !empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else if(isset($input['password'])) {
            unset($input['password']);
        }
        
        $meta = array('total' => count($user), 'count' => count($user), 'offset' => 0, 'last_updated' => $this->userRepository->lastUpdated(), 'status' => "User updated successfully.", 'error' => 'Success');
        
        $user = $this->userRepository->updateRich($input, $id);

        if (!$user) {
            $this->throwRecordNotFoundException("User not saved", ERROR_CODE_VALIDATION_FAILED);
        }
        
        $user = $this->userRepository->find($id);
                
        if (isset($input['role_id'])) {
            $user->detachAllRoles();
            $user->attachRole($input['role_id']);
        }

        return Response::json(ResponseManager::makeResult($user->toArray(), $meta), 201, [], JSON_NUMERIC_CHECK);
    }
    
    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id) {
        $user = $this->userRepository->find($id);
        
        if (empty($user)) {
            $this->throwRecordNotFoundException("User not found", ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $user = $this->userRepository->delete($id);
        
        $meta = array('total' => count($user), 'count' => count($user), 'offset' => 0, 'last_updated' => $this->userRepository->lastUpdated(), 'status' => "User deleted successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($id, $meta), 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * Display a listing of the Menu.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function me(Request $request) {
        $id = \Authorizer::getResourceOwnerId();

        // Update Profile
        if ($request->isMethod('post')) {
            $result = $this->update($id, $request);
            return $result;
        } 

        $input = $request->all();


        /**
         * Ganti find menggunakan search karena find tidak bisa mengguanakan with???
         * id harus integer : bug di windows
         */
        $result = $this->userRepository->search($input);
        $user = $result[0]->find(intval($id));
        
        if (empty($user)) {
            $this->throwRecordNotFoundException("User not found", ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $meta = array('total' => count($user), 'count' => count($user), 'offset' => 0, 'last_updated' => $this->userRepository->lastUpdated(), 'status' => "DisasterEvent retrieved successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($user->toArray(), $meta), 200, [], JSON_NUMERIC_CHECK);
    }

    public function login(Request $request) {
        $input = $request->all();
        
        $return = \Authorizer::issueAccessToken();
        
        return Response::json($return);
    }

    public function signup(Request $request) {
        if (sizeof(User::$rules) > 0) {
            $this->validateRequest($request, User::$rules);
        }
        
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        
        $user = $this->userRepository->create($input);

        if (isset($input['role'])) {
            if ($input['role'] === 'perorangan') 
                $user->attachRole(4);
            else if ($input['role'] === 'badan-hukum') 
                $user->attachRole(5);
        }
        

        // dd($user);

        $return = \Authorizer::issueAccessToken();
        
        return Response::json($return);
    }
}
