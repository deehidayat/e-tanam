<?php
namespace App\Http\Controllers\API\Common;

use App\Http\Requests;
use Mitul\Controller\AppBaseController;
use Mitul\Generator\Utils\ResponseManager;
use App\Models\Role;
use App\Models\PermissionRole;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Common\RoleRepository;
use Response;
use Schema;

class RoleAPIController extends AppBaseController
{
    
    /** @var  RoleRepository */
    private $roleRepository;
    
    function __construct(RoleRepository $roleRepo) {
        $this->roleRepository = $roleRepo;
        
        $this->middleware('oauth_permission');
        $this->beforeFilter('oauth', ['except' => []]);
    }
    
    /**
     * Display a listing of the Role.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request) {
        $input = $request->all();
        
        $result = $this->roleRepository->search($input);
        
        // Ambil Role User
        $userId = \Authorizer::getResourceOwnerId();
        $userLogin = \App\User::find($userId);

        // Filter hanya user yang ada dibawah user login saja
        $roles = $result['query']->where('level', '>', $userLogin->level())->get();
        
        $meta = array('total' => $result['total'], 'count' => count($roles), 'offset' => isset($input['offset']) ? (int)$input['offset'] : 0, 'last_updated' => $this->roleRepository->lastUpdated(), 'status' => "Roles retrieved successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($roles->toArray(), $meta), 200, [], JSON_NUMERIC_CHECK);
    }
    
    /**
     * Show the form for creating a new Role.
     *
     * @return Response
     */
    public function create() {
        
        //
        
    }
    
    /**
     * Store a newly created Role in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request) {
        if (sizeof(Role::$rules) > 0) {
            $this->validateRequest($request, Role::$rules);
        }
        $input = $request->all();

        $input['slug'] = str_slug($input['name'], '_');

        \DB::beginTransaction();
        try {
            $role = $this->roleRepository->create($input);
            
            if (isset($input['permission_ids'])) {
                $permissions = $input['permission_ids'];
                $permissionRoles = [];
                foreach ($permissions as $key => $value) {
                    $temp = PermissionRole::firstOrCreate(['permission_id' => $key, 'role_id' => $role->id]);
                    if($value == false && !empty($temp)) {
                        $temp->delete();
                    } else {
                        $temp->save();
                    }
                }
                // $role->permissions()->saveMany($permissionRoles);
            }
        } catch (Exception $e) {
            \DB::rollback();
        }
        \DB::commit();
        
        $meta = array('total' => 1, 'count' => 1, 'offset' => 0, 'last_updated' => $this->roleRepository->lastUpdated(), 'status' => "Role saved successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($role->toArray(), $meta), 201, [], JSON_NUMERIC_CHECK);
    }
    
    /**
     * Display the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request) {
        $input = $request->all();
        
        /**
         * Ganti find menggunakan search karena find tidak bisa mengguanakan with???
         */
        
        // $role = $this->roleRepository->find($id);
        $result = $this->roleRepository->search($input);
        $role = $result[0]->find(intval($id));
        
        if (empty($role)) {
            $this->throwRecordNotFoundException("DisasterEvent not found", ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $meta = array('total' => count($role), 'count' => count($role), 'offset' => 0, 'last_updated' => $this->roleRepository->lastUpdated(), 'status' => "DisasterEvent retrieved successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($role->toArray(), $meta), 200, [], JSON_NUMERIC_CHECK);
    }
    
    /**
     * Show the form for editing the specified Role.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        
        //
        
    }
    
    /**
     * Update the specified Role in storage.
     *
     * @param  int    $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request) {
        $role = $this->roleRepository->find($id);
        
        if (empty($role)) {
            $this->throwRecordNotFoundException("Role not found", ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $input = $request->all();

        $input['slug'] = str_slug($input['name'], '_');
        
        $meta = array('total' => count($role), 'count' => count($role), 'offset' => 0, 'last_updated' => $this->roleRepository->lastUpdated(), 'status' => "Role updated successfully.", 'error' => 'Success');
        
        \DB::beginTransaction();
        try {
            $role = $this->roleRepository->updateRich($input, $id);
            
            if (!$role) {
                $this->throwRecordNotFoundException("Role not saved", ERROR_CODE_VALIDATION_FAILED);
            }
            
            $role = $this->roleRepository->find($id);
            
            if (isset($input['permission_ids'])) {
                $permissions = $input['permission_ids'];
                $permissionRoles = [];
                foreach ($permissions as $key => $value) {
                    $temp = PermissionRole::firstOrCreate(['permission_id' => $key, 'role_id' => $role->id]);
                    if($value == false && !empty($temp)) {
                        $temp->delete();
                    } else {
                        $temp->save();
                    }
                }
            }
        } catch (Exception $e) {
            \DB::rollback();
        }
        \DB::commit();
        
        return Response::json(ResponseManager::makeResult($role->toArray(), $meta), 201, [], JSON_NUMERIC_CHECK);
    }
    
    /**
     * Remove the specified Role from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id) {
        $role = $this->roleRepository->find($id);
        
        if (empty($role)) {
            $this->throwRecordNotFoundException("Role not found", ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $role = $this->roleRepository->delete($id);
        
        $meta = array('total' => count($role), 'count' => count($role), 'offset' => 0, 'last_updated' => $this->roleRepository->lastUpdated(), 'status' => "Role deleted successfully.", 'error' => 'Success');
        
        return Response::json(ResponseManager::makeResult($id, $meta), 200, [], JSON_NUMERIC_CHECK);
    }
}
