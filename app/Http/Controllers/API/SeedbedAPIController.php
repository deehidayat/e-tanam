<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\API\Common\CRUDAPIController;
use Illuminate\Container\Container as Application;
// Ganti Class Repository nya  saja
use App\Libraries\Repositories\SeedbedRepository as Repository;
use App\Models\SeedbedDetail;
use Illuminate\Http\Request;

class SeedbedAPIController extends CRUDAPIController
{
    function __construct(Application $app, Repository $repo) {
        $this->repo = $repo;
        parent::__construct($app);
    }

    public function store(Request $request) {
        if (sizeof($this->getCreateRules()) > 0) {
            $this->validateRequest($request, $this->getCreateRules());
        }
        $input = $request->all();        
        $record = $this->repo->create($input);
        /**
         * Script untuk insert details
         */
        if (!empty($request->get('details'))) {         
        	$details = $request->get('details');
        	foreach ($details as $key => &$value) {
    			$value['seedbed_id'] = $record->id;
    	  	}  
    	  	SeedbedDetail::insert($details);
        }

        $meta = array('total' => 1, 'count' => 1, 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record saved successfully.', 'error' => 'Success');        
        return $this->response($record->toArray(), $meta, 201);
    }    

    public function show($id, Request $request) {
        $record = $this->repo->find($id);
        
        if (empty($record)) {
            $this->throwRecordNotFoundException('Record not found', ERROR_CODE_RECORD_NOT_FOUND);
        }
        $meta = array('total' => count($record), 'count' => count($record), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record retrieved successfully.', 'error' => 'Success');
        
        $result = array_merge($record->toArray(), ['details' => $record->details->toArray(), 'location' => $record->location->toArray()]);

        return $this->response($result, $meta);
    }

    public function update($id, Request $request) {
        $record = $this->repo->find($id);
        
        if (empty($record)) {
            $this->throwRecordNotFoundException('Record not found', ERROR_CODE_RECORD_NOT_FOUND);
        }
        
        $input = $request->all();
        
        $record = $this->repo->updateRich($input, $id);
        
        if (!$record) {
            $this->throwRecordNotFoundException('Record not saved', ERROR_CODE_VALIDATION_FAILED);
        }
        
        $record = $this->repo->find($id);

        /**
         * Script untuk insert details
         */
        if (!empty($request->get('details'))) {         
            $details = $request->get('details');
            $newDetails = [];
            foreach ($details as $key => &$value) {
                if (isset($value['id'])) {
                    if (isset($value['deleted']) && $value['deleted'] == true) {
                        SeedbedDetail::find($value['id'])->delete();
                    } else {
                        SeedbedDetail::find($value['id'])->update($value);
                    }
                } else {
                    $value['seedbed_id'] = $record->id;
                    $newDetails[] = $value;
                }
            }  
            if (count($newDetails)) {
                SeedbedDetail::insert($newDetails);
            }
        }

        $meta = array('total' => count($record), 'count' => count($record), 'offset' => 0, 'last_updated' => $this->repo->lastUpdated(), 'status' => 'Record updated successfully.', 'error' => 'Success');
        
        return $this->response($record->toArray(), $meta);
    }
    
}