<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController', ]);
Route::post('v1/password/email', 'Auth\PasswordController@postEmail');
Route::post('v1/password/reset', 'Auth\PasswordController@postReset');
Route::post('v1/signup', 'API\Common\UserAPIController@signup');


/** Common Route */
Route::resource('v1/permissions', 'API\Common\PermissionAPIController');
Route::resource('v1/roles', 'API\Common\RoleAPIController');
Route::resource('v1/users', 'API\Common\UserAPIController');
Route::post('v1/oauth/login', 'API\Common\UserAPIController@login');
Route::get('v1/me', 'API\Common\UserAPIController@me');
Route::post('v1/me', 'API\Common\UserAPIController@me');
// Route::post('v1/permissionRoles/bulkUpdate', 'API\Common\PermissionRoleAPIController@bulkUpdate');
// Route::resource('v1/permissionRoles', 'API\Common\PermissionRoleAPIController');
// Route::resource('v1/permissionUsers', 'API\Common\PermissionUserAPIController');
// Route::resource('v1/roleUsers', 'API\Common\RoleUserAPIController');

/** Master Data Route */
// Route::resource('v1/districts', 'API\DistrictAPIController');
Route::post('v1/plants/{id}', 'API\PlantAPIController@update');
Route::resource('v1/plants', 'API\PlantAPIController');
Route::resource('v1/emissions', 'API\EmissionAPIController');
Route::post('v1/plantings/{id}', 'API\PlantingAPIController@update');
Route::get('v1/plantings/statistic', 'API\PlantingAPIController@statistic');
Route::resource('v1/plantings', 'API\PlantingAPIController');
Route::resource('v1/seeds', 'API\SeedAPIController');
Route::resource('v1/seedbeds', 'API\SeedbedAPIController');
Route::resource('v1/stockopnames', 'API\StockOpnameAPIController');
Route::resource('v1/approvalseeds', 'API\ApprovalSeedAPIController');
Route::resource('v1/stockopnames', 'API\StockOpnameAPIController');
Route::resource('v1/locations', 'API\LocationAPIController');

Route::get('v1/dashboards/plantings', 'API\DashboardAPIController@plantings');
Route::get('v1/dashboards/seedbeds', 'API\DashboardAPIController@seedbeds');
Route::get('v1/dashboards/statistics', 'API\DashboardAPIController@statistics');
Route::get('v1/dashboards/summary', 'API\DashboardAPIController@summary');
Route::get('v1/dashboards/summary2', 'API\DashboardAPIController@summary2');
