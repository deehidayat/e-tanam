<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DistrictUser extends Model
{
    
    public $table = "district_users";
    
    public $primaryKey = "id";
    
    public $timestamps = true;
    
    public $fillable = ["district_id", "user_id"];
    
    public static $rules = ["district_id" => "require", "user_id" => "required"];
    
    // Touching Parent Timestamps
    protected $touches = ['district', 'district'];
    
    public function district() {
        return $this->belongsTo('App\Models\District', 'district_id', 'id');
    }
    
    public function district() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
