<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockOpname extends Model
{
    public $table = 'stock_opnames';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['seedbed_id', 'seedbed_detail_id', 'new_quantity'];
    
    public $hidden = ['deleted_at'];

    public $rules = [
        'seedbed_id' => 'required',
        'seedbed_detail_id' => 'required',
        'new_quantity' => 'integer|required'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function seedbed() {
        return $this->belongsTo('App\Models\Seedbed', 'seedbed_id', 'id');
    }

    public function seedbedDetail() {
        return $this->belongsTo('App\Models\SeedbedDetail', 'seedbed_detail_id', 'id');
    }
}
