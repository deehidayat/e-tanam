<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApprovalSeed extends Model
{
    public $table = 'approval_seeds';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['seed_id', 'seedbed_detail_id', 'quantity'];
    
    public $hidden = ['deleted_at'];

    public $rules = [
        'seed_id' => 'required',
        'seedbed_detail_id' => 'required',
        'quantity' => 'required|numeric|min:0',
    ];

    protected $with = ['seed.user', 'seedbedDetail.seedbed'];

    public function seed() {
        return $this->belongsTo('App\Models\Seed', 'seed_id', 'id');
    }

    public function seedbedDetail() {
        return $this->belongsTo('App\Models\SeedbedDetail', 'seedbed_detail_id', 'id');
    }
}