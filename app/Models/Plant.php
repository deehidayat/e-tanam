<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    public $table = 'plants';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['number', 'address', 'description', 'lon', 'lat', 'location_id', 'emission_id'];
    
    public $hidden = ['deleted_at'];

    public $rules = [
        'number' => 'required|unique:plants',
        'address' => 'required',
        'lon' => 'required',
        'lat' => 'required',
        'location_id' => 'required|exists:locations,id',
        'emission_id' => 'required|exists:emissions,id'
    ];

    public function photos() {
        return $this->hasMany('App\Models\Media', 'owner_id', 'id')->where('owner', 'plants');
    }

    public function location() {
        return $this->belongsTo('App\Models\Location', 'location_id', 'id');
    }

    public function emission() {
        return $this->belongsTo('App\Models\Emission', 'emission_id', 'id');
    }
    
}
