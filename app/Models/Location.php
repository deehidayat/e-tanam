<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public $table = 'locations';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    // public $fillable = ['address', 'lon', 'lat', 'wide'];
    
    public $hidden = ['deleted_at'];

}