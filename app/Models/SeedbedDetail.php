<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeedbedDetail extends Model
{
    public $table = 'seedbed_details';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['type', 'quantity', 'date'];
    
    public $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public $rules = [
        'type' => 'required',
        'quantity' => 'required',
        'date' => 'required',
        'seedbed_id' => 'required'
    ];

    public function seedbed() {
        return $this->belongsTo('App\Models\Seedbed', 'seedbed_id', 'id');
    }
}
