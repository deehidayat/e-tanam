<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planting extends Model
{
    public $table = 'plantings';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['planting_date', 'year', 'address', 'type', 'quantity', 'grow_percent', 'diameter', 'lon', 'lat', 'user_id', 'location_id', 'emission_id','sumber_dana'];
    
    public $hidden = ['deleted_at'];

    public $rules = [
        'year' => 'required',
        'address' => 'required',
        'quantity' => 'integer|required',
        'grow_percent' => 'required',
        'lon' => 'required',
        'lat' => 'required',
        'user_id' => 'required',
        'location_id' => 'required|exists:locations,id',
        'emission_id' => 'required|exists:emissions,id'		
    ];

    protected $appends = ['calculation'];

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function photos() {
        return $this->hasMany('App\Models\Media', 'owner_id', 'id')->where('owner', 'plantings');
    }

    public function location() {
        return $this->belongsTo('App\Models\Location', 'location_id', 'id');
    }

    public function emission() {
        return $this->belongsTo('App\Models\Emission', 'emission_id', 'id');
    }

    public function getCalculationAttribute() {
        $now = date('Y');
        return ($now - $this->year) * $this->emission->emission * $this->quantity * $this->grow_percent / 100;
    }
}
