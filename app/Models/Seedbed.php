<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seedbed extends Model
{
    public $table = 'seedbeds';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['address', 'lon', 'lat', 'wide', 'location_id'];
    
    public $hidden = ['deleted_at'];

    public $rules = [
        'address' => 'required',
        'wide' => 'required',
        'lon' => 'required',
        'lat' => 'required',
        'location_id' => 'required|exists:locations,id'
    ];

    public function details() {
        return $this->hasMany('App\Models\SeedbedDetail', 'seedbed_id', 'id');
    }

    public function location() {
        return $this->belongsTo('App\Models\Location', 'location_id', 'id');
    }
}