<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emission extends Model
{
    
    public $table = 'emissions';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['name', 'emission', 'type'];
    
    public $rules = [
        'name' => 'required',
        'emission' => 'required',
        'type' => 'required|in:Fast Growing,Slow Growing,Non Fast dan Slow Growing'
    ];
}