<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public $table = 'medias';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['url', 'caption', 'owner', 'owner_id'];
    
    public $hidden = ['owner', 'owner_id', 'created_at', 'updated_at', 'deleted_at'];

    public $rules = [
        'url' => 'required|unique:medias',
        'owner' => 'required|in:plants,users'
    ];
}
