<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FastGrowingEmission extends Model
{
    
    public $table = 'fast_growing_emissions';
    
    public $primaryKey = 'id';
    
    public $timestamps = true;
    
    public $fillable = ['name', 'emission'];
    
    public $hidden = ['deleted_at'];

    public $rules = [
        'name' => 'required',
        'emission' => 'required'
    ];
}