<?php
namespace App\Libraries\Repositories;

use App\Libraries\Repositories\Common\CRUDRepository;
// Ganti Class Modelnya nya  saja
use App\Models\Seed as Model;

class SeedRepository extends CRUDRepository {

    public function model() {
        return Model::class;
    }    

}