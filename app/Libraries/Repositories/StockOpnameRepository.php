<?php
namespace App\Libraries\Repositories;

use App\Libraries\Repositories\Common\CRUDRepository;
// Ganti Class Modelnya nya  saja
use App\Models\StockOpname as Model;

class StockOpnameRepository extends CRUDRepository {

    public function model() {
        return Model::class;
    }    

}