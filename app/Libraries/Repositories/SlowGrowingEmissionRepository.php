<?php
namespace App\Libraries\Repositories;

use App\Libraries\Repositories\Common\CRUDRepository;
// Ganti Class Modelnya nya  saja
use App\Models\SlowGrowingEmission as Model;

class SlowGrowingEmissionRepository extends CRUDRepository {
    
    public function model() {
        return Model::class;
    }
    
}
