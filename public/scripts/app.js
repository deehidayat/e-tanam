'use strict';

/**
 * @ngdoc overview
 * @name minovateApp
 * @description
 * # minovateApp
 *
 * Main module of the application.
 */
angular
  .module('minovateApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'picardy.fontawesome',
    'ui.bootstrap',
    'ui.router',
    'ui.utils',
    'angular-loading-bar',
    'angular-momentjs',
    'lazyModel',
    'toastr',
    'smart-table',
    'oc.lazyLoad',
    'angular-flot',
    'easypiechart',
    'openlayers-directive',
    'ngFileUpload',
    'cgBusy',
    'localytics.directives'
  ])
  .value('cgBusyDefaults',{
    message:'Please wait..',
    backdrop: true,
    // templateUrl: 'my_custom_template.html',
    delay: 0,
    minDuration: 700,
    // wrapperClass: 'my-class my-class2'
  })
  .run(['$rootScope', '$state', '$stateParams', 'appAuth', function($rootScope, $state, $stateParams, appAuth) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeSuccess', function(event, toState) {

      event.targetScope.$watch('$viewContentLoaded', function () {

        angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);

        setTimeout(function () {
          angular.element('#wrap').css('visibility','visible');

          if (!angular.element('.dropdown').hasClass('open')) {
            angular.element('.dropdown').find('>ul').slideUp();
          }
        }, 200);
      });
      $rootScope.containerClass = toState.containerClass || 'xhz-menu xboxed-layout';
    });

    /**
     * RBAC
     */
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        // console.info('You access state', toState, fromState.url );
        /**
         * Cegah akses ke halaman anon untuk user yang sudah login
         */
        if (toState.data && toState.data.accessLevel === AUTHConfig.accessLevels.anon && appAuth.authenticated === true) {
            event.preventDefault();
            angular.element('#pageloader').toggleClass('hide animate');
            return $state.go('app.dashboard');
        } else if (toState.data && !appAuth.authorize(toState.data.accessLevel || AUTHConfig.accessLevels.public)) {
            event.preventDefault();
            angular.element('#pageloader').toggleClass('hide animate');
            console.warn('Page Access Denied', toState, fromState.url);
            return $state.go(toState.data.loginState, {backTo: $state.href(toState.name, null, {absolute: true})});
        }
    });

  }])

  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider',  function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

    // use the HTML5 History API
    $locationProvider.html5Mode(true);

    // showErrorsConfigProvider.showSuccess(false);

    /**
     * Setup Loading Bar
     */
    // cfpLoadingBarProvider.includeSpinner = false;
    // cfpLoadingBarProvider.includeBar = true;

    /**
     * Setup CORS
     */
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    /**
     * Setup API
     */
    $httpProvider.defaults.headers.get = {
        'Accept' : '*/*',
        'Content-Type' : 'application/json;charset=utf-8'
    };

    $httpProvider.defaults.headers.post = {
        'Accept' : '*/*',
        'Content-Type' : 'application/json;charset=utf-8'
    };

    $httpProvider.defaults.headers['delete'] = {
        'Accept' : '*/*',
        'Content-Type' : 'application/json;charset=utf-8'
    };

    $httpProvider.defaults.headers.patch = {
        'Accept' : '*/*',
        'Content-Type' : 'application/json;charset=utf-8'
    };

    $urlRouterProvider.otherwise('/app/dashboard');

    $stateProvider
    .state('app', {
      abstract: true,
      url: '/app',
      templateUrl: 'views/app.html',
      data: {
        loginState: 'app.login'
      }
    })
    //dashboard
    .state('app.dashboard', {
      url: '/dashboard',
      controller: 'DashboardCtrl',
      templateUrl: 'views/dashboard.html',
      resolve: {
        // plugins: ['$ocLazyLoad', function($ocLazyLoad) {
        //   return $ocLazyLoad.load([
        //     'scripts/vendor/datatables/datatables.bootstrap.min.css'
        //   ]);
        // }]
      }
    })
    .state('app.login', {
      url: '/login?backTo=null',
      controller: 'LoginCtrl',
      templateUrl: 'views/login.html',
      data: {
          accessLevel: AUTHConfig.accessLevels.anon
      }
    })
    .state('app.resetpassword', {
      url: '/password/email',
      controller: 'ResetPasswordCtrl',
      templateUrl: 'views/forgotpass.html',
      data: {
          accessLevel: AUTHConfig.accessLevels.anon
      }
    })
    .state('app.newpassword', {
      url: '/password/reset/:token?email',
      controller: 'ResetPasswordCtrl',
      templateUrl: 'views/resetpass.html',
      data: {
          accessLevel: AUTHConfig.accessLevels.anon
      }
    })
    .state('app.signup', {
      url: '/daftar',
      controller: 'SignUpCtrl',
      templateUrl: 'views/signup.html',
      data: {
          accessLevel: AUTHConfig.accessLevels.anon
      }
    });
  }]);

