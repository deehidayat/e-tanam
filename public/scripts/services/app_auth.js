'use strict';

(function(exports) {
    var userRoles = {
        public : 1,
        user : 2,
        admin : 4,
    };
    exports.userRoles = userRoles;
    exports.accessLevels = {
        anon : userRoles.public,
        public : userRoles.public | userRoles.user | userRoles.admin,
        user : userRoles.user | userRoles.admin,
        admin : userRoles.admin,
    };
})( typeof exports === 'undefined' ? window.AUTHConfig = {} : exports);

/**
 * @ngdoc service
 * @name minovateApp.appAuth
 * @description
 * # appAuth
 * Factory in the minovateApp.
 */
angular.module('minovateApp')
  .factory('appAuth', ['$q', '$store', '$http', 'appConfig', 'appPopup', function ($q, $store, $http, appConfig, appPopup) {

    function UserData() {
        this.authenticated = false;
        this.role = AUTHConfig.userRoles.public;
        // this.user = null;
        this.token = null;
        this.expires = null;
        this.profile = null;
        this.permissions = [];
        this._sync();
    }

    UserData.prototype = {
        _default: function() {
            return {
                authenticated:false,
                role:AUTHConfig.userRoles.public,
                // user:null,
                profile:null,
                permissions: [],
                token:null,
                expires:null,
                districts: [],
                districtIds: []
            };
        },

        _sync: function() {
            // angular.forEach(this._default(), function(val, key) {
            //     // if (key==='user' || key==='customer') {
            //     if ((key==='user' || key==='customer') && $store.get(key)) {
            //         this[key] = JSON.parse($store.get(key));
            //     } else if ($store.get(key)) {
            //         this[key] = $store.get(key);
            //     }
            // }, this);

            angular.forEach(this._default(), function(val, key) {
                this[key] = $store.get(key);
            }, this);

            if (this._isExpired()) this._reset();
        },

        _reset: function() {
            angular.forEach(this._default(), function(val, key) {
                $store.set(key, val);
                this[key] = val;
            }, this);
        },

        _isExpired: function() {
            var now = +new Date, expires = this.expires, expired = now >= this.expires;
            if(this.expires && expired)
                console.log('token expired', new Date(this.expires));
            return expired;
        },

        authorize: function(accessLevel, role) {
            /**
             * Default Role apabila $cookies.UserService belum ada
             */
            if (!angular.isDefined(role)) {
                role = this.role || AUTHConfig.userRoles.public;
            }

            /**
             * Apabila token expired, reset data
             */
            if (accessLevel != AUTHConfig.accessLevels.public && accessLevel != AUTHConfig.accessLevels.anon  && this._isExpired()) {
                this._reset();
                return false;
            }

            /**
             * Check Page Access Level vs User Role
             */
            if (angular.isNumber(accessLevel) && angular.isNumber(role)) {
                // console.log('authorize', accessLevel, role, accessLevel & role);
                return accessLevel & role;
            } else {
                return false;
            }
        },

        logout: function() {
            var me = this;
            /**
             * Logout via API /session/logout
             */
            // var http = $http.post(appConfig.api.get('baseUrl') +'/sessions/logout',{
            //     token : me.token,
            // });
            // http.success(function(response, status){
                me._reset();
            // });
            // return http;
            
            var http = $q.defer();
            setTimeout(function(){
                http.resolve();
            }, 500);
            return http.promise;
        },

        login: function(data, type) {
            var me = this;

            /**
             * Login via API /session/login
             */
            var http = $http.post(appConfig.api.get('baseUrl') +'/oauth/login',angular.extend({
                username : data.username,
                password : data.password
            }, appConfig.api.get('auth')));
            http.then(function(response, status){
                me.setData({
                    authenticated: true,
                    role: AUTHConfig.userRoles.user,
                    token: response.data.access_token,
                    expires: (new Date()).setSeconds(response.data.expires_in - 60)
                });
            });
            return http;
        },

        requestReset: function(email) {
            var me = this;

            /**
             * Login via API /session/login
             */
            var http = $http.post(appConfig.api.get('baseUrl') + '/password/email',angular.extend({
                email : email
            }));
            return http;
        },

        resetPassword: function(data) {
            var me = this;

            /**
             * Login via API /session/login
             */
            var http = $http.post(appConfig.api.get('baseUrl') +'/password/reset', data);
            return http;
        },

        signup: function(data) {
            var me = this;

            /**
             * Login via API /session/login
             */
            var http = $http.post(appConfig.api.get('baseUrl') +'/signup', angular.extend(data, appConfig.api.get('auth'), {
                username: data.email
            }));
            http.then(function(response, status){
                me.setData({
                    authenticated: true,
                    role: AUTHConfig.userRoles.user,
                    token: response.data.access_token,
                    expires: (new Date()).setSeconds(response.data.expires_in - 60)
                });
            });
            return http;
        },


        setData: function(data) {
            angular.forEach(data, function(value, key) {
                $store.set(key, value);
            });
            this._sync();
        },

        getMe: function(data) {
          var self = this, http = $http.get(appConfig.api.get('baseUrl') + '/me', {
            params: angular.extend({
              access_token: self.token,
              'with': '["roles.permissions", "districts"]'
            }, data)
          }, {
            cache: false
          });
          http.then(function(response, status) {
            // console.info(response, self);
            var permissions = response.data.data.roles && response.data.data.roles.length ? response.data.data.roles[0].permissions : [];
            self.setData({
              profile: response.data.data,
              permissions: permissions.length ? permissions.map(function(item){ return item.slug; }) : [],
              districts: response.data.data.districts || [],
              districtIds: response.data.data.districts.map(function(item){ return item.id; })
            });
          }, function(response, status) {
            console.info(response, status);
          })
          return http;
        },

        updateMe: function(data) {
          var self = this, http = $http.post(appConfig.api.get('baseUrl') + '/me', angular.extend({
              access_token: self.token
            }, data), {
            params: {  
              'with': '["roles.permissions", "districts"]'
            }
          });
          return http;
        },

        hasAccess: function(keys) {
            var result = false;
            var criteria =  this.permissions.length ? new RegExp(this.permissions.join('|')) : false;
            if (criteria)
            {
              if (angular.isArray(keys))
              {
                keys.forEach(function(key, idx)
                {
                  // if(WrpTier3.Util.isHiddenFeature(key)) {
                  //   result = result || false;
                  // } else {
                    result = result || criteria.test(key);
                  // }
                });
              }
              else
              {
                // if(WrpTier3.Util.isHiddenFeature(featureKey)) {
                //   result = false;
                // } else {
                  result = criteria.test(keys);
                // }
              }
            }
            return result;
        },

        hasAccessDistrict: function(id) {
            return this.districtIds ? this.districtIds.indexOf(parseInt(id)) !== -1 : true;
        }

    };

    var userData = new UserData();
    return userData;

}]).directive('permissionsRoot', ['appAuth', function(appAuth) {
    return {
        restrict: 'A',
        controller: ['$scope', function($scope){
        }],
        scope: {
            allowed: '=permissionsRoot'
        },
        link: function postLink(scope, element) {
            var childPermissions = element.find('.permissions');
            var keys = [];
            angular.forEach(childPermissions, function(childEl){
                var key = angular.element(childEl).data('permissions');
                if (angular.isArray(key)) {
                    keys = angular.extend(keys, key);
                } else {
                    keys.push(key);
                }
            });
            scope.$watch(function(){
                return appAuth.hasAccess(keys);
            }, function(allowed){
                scope.allowed = allowed;
                if(!allowed) {
                    if(scope.mode == 'disabled') {
                        element.disable();
                    } else {
                        element.hide();
                    }
                } else {
                    element.show();
                }
            })
        }
    };
}]).directive('permissions', ['appAuth', function(appAuth) {
    return {
        restrict: 'A',
        controller: ['$scope', function($scope){
        }],
        scope: {
            keys: '@permissions',
            mode: '@controlMode',
            district: '@district'
        },
        link: function postLink(scope, element) {
            element.addClass('permissions');
            scope.$watch(function(){
                return appAuth.hasAccess(scope.keys) && (!scope.district || appAuth.hasAccessDistrict(scope.district));
            }, function(allowed){
                if(!allowed) {
                    if(scope.mode == 'disabled') {
                        element.disable();
                    } else {
                        element.hide();
                    }
                } else {
                    element.show();
                }
            })
        }
    };
}]);
