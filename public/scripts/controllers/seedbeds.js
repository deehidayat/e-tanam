'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:SeedbedsCtrl
 * @description
 * # SeedbedsCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.seedbeds', {
        url: '/persemaian',
        abstract: true,
        controller: 'SeedbedsCtrl',
        templateUrl: 'views/seedbeds/main.html'
    }).state('app.seedbeds.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/seedbeds/index.html',
                controller: 'SeedbedsIndexCtrl'
            }
        }
    }).state('app.seedbeds.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/seedbeds/form.html',
                controller: 'SeedbedsAddCtrl'
            }
        },
        resolve: {
            seedbedsDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.seedbeds.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/seedbeds/form.html',
                controller: 'SeedbedsAddCtrl'
            }
        },
        resolve: {
            seedbedsDetail: ['$stateParams', '$q', 'storeSeedbeds', 'appAuth', function($stateParams, $q, storeSeedbeds, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeSeedbeds.get({
                        id: $stateParams.id,
                        access_token: appAuth.token
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeSeedbeds', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/seedbeds/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('SeedbedsCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Lokasi Persemaian',
        route: 'seedbeds',
        subtitle: ''
    };
}]).controller('SeedbedsIndexCtrl', ['$scope', 'storeSeedbeds', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeSeedbeds, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset()
        };
        // Filtering
        if ($scope.query) params.filter = JSON.stringify([{
            field: 'address',
            operator: 'like',
            value: $scope.query
        }]);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeSeedbeds.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/seedbeds/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('SeedbedsAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeSeedbeds', 'seedbedsDetail', 'appPopup', 'Upload', 'storeLocations', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeSeedbeds, seedbedsDetail, appPopup, Upload, storeLocations) {
    var isContinueAdd = false;
    $scope.data = seedbedsDetail && seedbedsDetail.data ? seedbedsDetail.data : {};
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if (!$scope.data.id) {
            $scope.promise = storeSeedbeds.save(angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id
            }), function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.coord = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storeSeedbeds.update({
                id: $scope.data.id
            }, angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id
            }), function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };
    
    // $scope.users = users;
    $scope.coord = seedbedsDetail && seedbedsDetail.data ? {
        lon: seedbedsDetail.data.lon,
        lat: seedbedsDetail.data.lat
    } : {};

    // Details
    $scope.data.details = $scope.data.details && $scope.data.details.length ? $scope.data.details : [{}];
    $scope.opened = [false];

    $scope.removeDetail = function(detail, $index) {
        if (!detail.id) {
            $scope.data.details.splice($index, 1);
        } else {
            detail.deleted = !detail.deleted;
        }
    }
    
    // Location
    $scope.location = $scope.data.location ? $scope.data.location : '';
    $scope.removeLocation = function() {
        $scope.location = null;
    }
    var getLocation = function(query, type, offset, limit) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: limit || 20,
            offset: offset || 0,
            filter: []
        };
        // Filtering
        if (query) {
            if (angular.isString(query)) {
                params.filter.push({
                    field: 'name',
                    operator: 'like',
                    value: query
                });
            } else if (angular.isObject(query)) {
                for (var key in query) {
                    params.filter.push({
                        field: key,
                        operator: '=',
                        value: query[key]
                    });
                }
            }
        } 
        if (type === 'city') {
            params.filter.push({
                field: 'province_id',
                operator: '=',
                value: '32'
            });
            params.filter.push({
                field: 'city_id',
                operator: '!=',
                value: ''
            });
            params.filter.push({
                field: 'district_id',
                operator: '=',
                value: ''
            });
        } else if (type === 'district') {
            params.filter.push({
                field: 'district_id',
                operator: '!=',
                value: ''
            });  
            params.filter.push({
                field: 'village_id',
                operator: '=',
                value: ''
            });  
        } else {
            params.filter.push({
                field: 'village_id',
                operator: '!=',
                value: ''
            });  
        }
        if (params.filter.length) params.filter = JSON.stringify(params.filter);
        params.order = JSON.stringify([{
            field: 'name',
            sort: 'asc',
        }]);
        return storeLocations.query(params).$promise.then(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            if (response.data) {
                return response.data;
            }
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    // Kab/Kota
    $scope.city = null;
    $scope.cities = [];
    // Kecamatan
    $scope.district = null;
    $scope.districts = [];
    // Desa
    $scope.village = null;
    $scope.villages = [];
    // Listeners
    getLocation(null, 'city', 0, 1000).then(function(data){
        $scope.cities = data;
    });
    $scope.$watch('city', function(newVal, oldVal) {
        if (newVal) {
            getLocation({ city_id: newVal.city_id }, 'district', 0, 1000).then(function(data){
                $scope.districts = data;
                $scope.district = null;
                $scope.village = null;
            });
        }
    });
    $scope.$watch('district', function(newVal, oldVal) {
        if (newVal) {
            getLocation({ district_id: newVal.district_id }, 'village', 0, 1000).then(function(data){
                $scope.villages = data;
                $scope.village = null;
            });
        }
    });
    
}]);