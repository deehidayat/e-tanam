'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.users', {
        url: '/pengguna',
        abstract: true,
        controller: 'UsersCtrl',
        templateUrl: 'views/users/main.html'
    }).state('app.users.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/users/index.html',
                controller: 'UsersIndexCtrl'
            }
        }
    }).state('app.users.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/users/form.html',
                controller: 'UsersAddCtrl'
            }
        },
        resolve: {
            roles: ['storeRoles', '$q', 'appAuth', function(storeRoles, $q, appAuth) {
                var defer = $q.defer();
                storeRoles.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            seedbeds: ['storeSeedbeds', '$q', 'appAuth', function(storeSeedbeds, $q, appAuth) {
                var defer = $q.defer();
                storeSeedbeds.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            usersDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.users.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/users/form.html',
                controller: 'UsersAddCtrl'
            }
        },
        resolve: {
            roles: ['storeRoles', '$q', 'appAuth', function(storeRoles, $q, appAuth) {
                var defer = $q.defer();
                storeRoles.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            seedbeds: ['storeSeedbeds', '$q', 'appAuth', function(storeSeedbeds, $q, appAuth) {
                var defer = $q.defer();
                storeSeedbeds.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            usersDetail: ['$stateParams', '$q', 'storeUsers', 'appAuth', function($stateParams, $q, storeUsers, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeUsers.get({
                        id: $stateParams.id,
                        access_token: appAuth.token
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeUsers', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/users/:id', {
        id: '@id',
        'with': '["roles"]'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('UsersCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Pengguna',
        route: 'users',
        subtitle: ''
    };
}]).controller('UsersIndexCtrl', ['$scope', 'storeUsers', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeUsers, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset()
        };
        // Filtering
        if ($scope.query) params.filter = JSON.stringify([{
            "field": "name",
            "operator": "like",
            "value": $scope.query
        }]);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeUsers.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/users/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('UsersAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeUsers', 'usersDetail', 'appPopup', 'roles', 'seedbeds', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeUsers, usersDetail, appPopup, roles, seedbeds) {
    $scope.roles = roles;
    $scope.seedbeds = seedbeds;

    var isContinueAdd = false;
    $scope.data = usersDetail && usersDetail.data ? usersDetail.data : {};
    if ($scope.data.roles && $scope.data.roles.length) {
        $scope.data.role_id = $scope.data.roles[0].id;
    }
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        angular.element('.tile').addClass('refreshing');
        if (!$scope.data.id) {
            storeUsers.save(angular.extend($scope.data, {
                access_token: appAuth.token
            }), function(response, status) {
                angular.element('.tile.refreshing').removeClass('refreshing');
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.confirm_password = null;
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                angular.element('.tile.refreshing').removeClass('refreshing');
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            storeUsers.update({
                id: $scope.data.id
            }, angular.extend($scope.data, {
                access_token: appAuth.token
            }), function(response, status) {
                angular.element('.tile.refreshing').removeClass('refreshing');
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                angular.element('.tile.refreshing').removeClass('refreshing');
                return appPopup.showResponseAPI(response, status);
            });
        }
    };
}]);