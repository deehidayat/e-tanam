'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').controller('LoginCtrl', ['$scope', 'appConfig', 'appAuth', 'appPopup', '$state', function($scope, appConfig, appAuth, appPopup, $state) {
    $scope.user = {
        username: null,
        password: null
    };
    $scope.login = function() {
        $scope.promise = appAuth.login($scope.user).then(function(response, status) {
            $scope.mainPromise = appAuth.getMe().then(function(responseMe){
                if ($state.params.backTo && false) {
                    var href = decodeURIComponent($state.params.backTo);
                    location.href = href == '/' ? $state.params.backTo.substr(1) : href;
                } else {
                    $state.go('app.dashboard');
                }
            }, function() {
                
            });
        }, function(response, status) {
            response.data = response.data || {};
            return appPopup.toast(response.data.error_description, undefined, 'error');
        });
    };
}]).controller('ResetPasswordCtrl', ['$scope', 'appConfig', 'appAuth', 'appPopup', '$state', function($scope, appConfig, appAuth, appPopup, $state) {
    $scope.data = {
        email: $state.params.email,
        token: $state.params.token
    };

    $scope.onRequestResetPassword = function(email) {
        $scope.promise = appAuth.requestReset(email).then(function(response, status) {
            return appPopup.toast(response.data.status, undefined, 'success');
        }, function(response, status) {
            return appPopup.toast(response.data.error_description, undefined, 'error');
        });
    }

    $scope.onResetPassword = function(data) {
        $scope.promise = appAuth.resetPassword(data).then(function(response, status) {
            appPopup.toast('Your password has reseted', undefined, 'success');
            $state.go('app.login');
        }, function(response, status) {
            return appPopup.toast(response.data.error_description, undefined, 'error');
        });
    }
}]).controller('SignUpCtrl', ['$scope', 'appConfig', 'appAuth', 'appPopup', '$state', function($scope, appConfig, appAuth, appPopup, $state) {
    $scope.step = 1;
    $scope.next = function () {
        $scope.step++;
    };
    $scope.back = function () {
        if ($scope.step === 1) {
            return $state.go('app.login');
        }
        $scope.step--;
    };

    $scope.data = {
        // name: 'Dede Hidayat',
        // email: 'dedehidayat@gmail.com',
        // password: 'dedehidayat@gmail.com',
        // password_confirmation: 'dedehidayat@gmail.com',
        role: 'perorangan',
        // address: 'Jl. Cipanas',
        // phone_number: '085722220287',
        // legal_entity_type: 'Koperasi',
        // id_number: '321212880001',
        // contact_person: 'Wew'
    };
    $scope.submit = function() {
        console.log('submit', $scope.data);
        $scope.promise = appAuth.signup($scope.data).then(function(response, status) {
            $scope.mainPromise = appAuth.getMe().then(function(responseMe){
                if ($state.params.backTo && false) {
                    var href = decodeURIComponent($state.params.backTo);
                    location.href = href == '/' ? $state.params.backTo.substr(1) : href;
                } else {
                    $state.go('app.dashboard');
                }
            }, function(response) {
                response.data = response.data || {};
                return appPopup.toast(response.data.error_description, undefined, 'error');
            });
        }, function(response, status) {
            response.data = response.data || {};
            return appPopup.toast(response.data.error_description, undefined, 'error');
        });
    };
}]);
