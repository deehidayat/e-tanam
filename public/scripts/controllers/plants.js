'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:PlantsCtrl
 * @description
 * # PlantsCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.plants', {
        url: '/deskripsi-tanaman',
        abstract: true,
        controller: 'PlantsCtrl',
        templateUrl: 'views/plants/main.html'
    }).state('app.plants.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/plants/index.html',
                controller: 'PlantsIndexCtrl'
            }
        }
    }).state('app.plants.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/plants/form.html',
                controller: 'PlantsAddCtrl'
            }
        },
        resolve: {
            emissions: ['storeEmissions', '$q', 'appAuth', function(storeEmissions, $q, appAuth) {
                var defer = $q.defer();
                storeEmissions.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            plantsDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.plants.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/plants/form.html',
                controller: 'PlantsAddCtrl'
            }
        },
        resolve: {
            emissions: ['storeEmissions', '$q', 'appAuth', function(storeEmissions, $q, appAuth) {
                var defer = $q.defer();
                storeEmissions.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            plantsDetail: ['$stateParams', '$q', 'storePlants', 'appAuth', function($stateParams, $q, storePlants, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storePlants.get({
                        id: $stateParams.id,
                        access_token: appAuth.token,
                        'with': "['location']"
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storePlants', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/plants/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('PlantsCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Deskripsi Tanaman',
        route: 'plants',
        subtitle: ''
    };
}]).controller('PlantsIndexCtrl', ['$scope', 'storePlants', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storePlants, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset(),
            'with': '["emission"]'
        };
        // Filtering
        if ($scope.query) params.filter = JSON.stringify([{
            field: 'number',
            operator: 'like',
            value: $scope.query
        }]);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storePlants.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/plants/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('PlantsAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storePlants', 'plantsDetail', 'appPopup', 'Upload', 'storeLocations', 'emissions', function($scope, appConfig, $q, appAuth, $state, $stateParams, storePlants, plantsDetail, appPopup, Upload, storeLocations, emissions) {
    var isContinueAdd = false;
    $scope.data = plantsDetail && plantsDetail.data ? plantsDetail.data : {};
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if ($scope.files.length) {
            return doUploadAll();
        }
        if (!$scope.data.id) {
            $scope.promise = storePlants.save(angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id,
                emission_id: $scope.emission.id
            }), function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.coord = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storePlants.update({
                id: $scope.data.id
            }, angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id,
                emission_id: $scope.emission.id,
                oldPhotos: $scope.oldPhotos.map(function(item){
                    return {
                        id: item.id,
                        caption: item.caption,
                        deleted: item.deleted
                    }
                })
            }), function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };
    
    $scope.coord = plantsDetail && plantsDetail.data ? {
        lon: plantsDetail.data.lon,
        lat: plantsDetail.data.lat
    } : {};

    // Uploader
    $scope.oldPhotos = $scope.data.photos;
    delete $scope.data.photos;
    $scope.files = [];
    $scope.removeFromQueue = function(index) {
        $scope.files.splice(index, 1);
    };
    $scope.onFileSelect = function(files) {
        files && files.forEach && files.forEach(function(file, key) {
            $scope.files.push({
                id: key,
                file: file
            });
        });
    };
    function doUploadAll(isContinue) {
        Upload.upload({
            url: $scope.data.id ? '/api/v1/plants/' + $scope.data.id : '/api/v1/plants',
            method: $scope.data.id ? 'POST' : 'POST',
            data: angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id,
                emission_id: $scope.emission.id
            }, {
                photos: $scope.files.map(function(item) {
                    return item.file;
                }),
                captions: $scope.files.map(function(item) {
                    return item.caption || item.file.name;
                })
            })
        }).then(function(response) {
            if (isContinueAdd) {
                $scope.data = {};
                $scope.coord = {};
                $scope.form.$setPristine();
                $scope.files = [];
            } else {
                if ($state.params.backTo) {
                    $state.go($state.params.backTo)
                } else {
                    $state.go('app.' + $scope.pageInformation.route + '.index')
                }
            }
            return appPopup.showResponseAPI(response, response.status);
        }, function(response) {
            return appPopup.showResponseAPI(response, response.status);
        }, function(evt) {
            $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    }
    $scope.clearExt = function(val) {
        return val.replace(/\.([a-zA-Z]{3,})/,'');
    }

    // Location
    $scope.location = $scope.data.location ? $scope.data.location : '';
    $scope.removeLocation = function() {
        $scope.location = null;
    }
    var getLocation = function(query, type, offset, limit) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: limit || 20,
            offset: offset || 0,
            filter: []
        };
        // Filtering
        if (query) {
            if (angular.isString(query)) {
                params.filter.push({
                    field: 'name',
                    operator: 'like',
                    value: query
                });
            } else if (angular.isObject(query)) {
                for (var key in query) {
                    params.filter.push({
                        field: key,
                        operator: '=',
                        value: query[key]
                    });
                }
            }
        } 
        if (type === 'city') {
            params.filter.push({
                field: 'province_id',
                operator: '=',
                value: '32'
            });
            params.filter.push({
                field: 'city_id',
                operator: '!=',
                value: ''
            });
            params.filter.push({
                field: 'district_id',
                operator: '=',
                value: ''
            });
        } else if (type === 'district') {
            params.filter.push({
                field: 'district_id',
                operator: '!=',
                value: ''
            });  
            params.filter.push({
                field: 'village_id',
                operator: '=',
                value: ''
            });  
        } else {
            params.filter.push({
                field: 'village_id',
                operator: '!=',
                value: ''
            });  
        }
        if (params.filter.length) params.filter = JSON.stringify(params.filter);
        params.order = JSON.stringify([{
            field: 'name',
            sort: 'asc',
        }]);
        return storeLocations.query(params).$promise.then(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            if (response.data) {
                return response.data;
            }
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    // Kab/Kota
    $scope.city = null;
    $scope.cities = [];
    // Kecamatan
    $scope.district = null;
    $scope.districts = [];
    // Desa
    $scope.village = null;
    $scope.villages = [];
    // Listeners
    getLocation(null, 'city', 0, 1000).then(function(data){
        $scope.cities = data;
    });
    $scope.$watch('city', function(newVal, oldVal) {
        if (newVal) {
            getLocation({ city_id: newVal.city_id }, 'district', 0, 1000).then(function(data){
                $scope.districts = data;
                $scope.district = null;
                $scope.village = null;
            });
        }
    });
    $scope.$watch('district', function(newVal, oldVal) {
        if (newVal) {
            getLocation({ district_id: newVal.district_id }, 'village', 0, 1000).then(function(data){
                $scope.villages = data;
                $scope.village = null;
            });
        }
    });
    
    // Faktor Emisi
    $scope.emissions = emissions;
    $scope.emission = $scope.data.emission_id ? emissions.filter(function(emission){
        return emission.id == $scope.data.emission_id;
    })[0] : null;
}]).directive('ngThumb', ['$window', function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
            var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };
    return {
        restrict: 'A',
        template: '<img/>',
        // template: '<canvas/>',
        replace: true,
        link: function(scope, element, attributes) {
            if (!helper.support) return;
            var params = scope.$eval(attributes.ngThumb);
            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;
            var canvas = element.find('canvas');
            var reader = new FileReader();
            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                // var img = new Image();
                // img.onload = onLoadImage;
                // img.src = event.target.result;
                element.attr('src', event.target.result);
            }

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({
                    width: width,
                    height: height
                });
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            }
        }
    };
}]);