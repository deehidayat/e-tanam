'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:StockOpnamesCtrl
 * @description
 * # StockOpnamesCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.stockopnames', {
        url: '/stock-opname',
        abstract: true,
        controller: 'StockOpnamesCtrl',
        templateUrl: 'views/stockopnames/main.html'
    }).state('app.stockopnames.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/stockopnames/index.html',
                controller: 'StockOpnamesIndexCtrl'
            }
        }
    }).state('app.stockopnames.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/stockopnames/form.html',
                controller: 'StockOpnamesAddCtrl'
            }
        },
        resolve: {
            seedbeds: ['storeSeedbeds', '$q', 'appAuth', function(storeSeedbeds, $q, appAuth) {
                var defer = $q.defer();
                storeSeedbeds.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            stockopnamesDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.stockopnames.detail', {
        url: '/detail/:id',
        views: {
            'page-content': {
                templateUrl: 'views/stockopnames/detail.html',
                controller: 'StockOpnamesDetailCtrl'
            }
        },
        resolve: {
            stockopnamesDetail: ['$stateParams', '$q', 'storeStockOpnames', 'appAuth', function($stateParams, $q, storeStockOpnames, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeStockOpnames.query({
                        access_token: appAuth.token,
                        filter: JSON.stringify([{
                            field: 'number',
                            value: $stateParams.id,
                            operator: '='
                        }]),
                        'with': '["user", "seedbed", "seedbedDetail"]'
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeStockOpnames', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/stockopnames/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('StockOpnamesCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Stock Opname',
        route: 'stockopnames',
        subtitle: ''
    };
}]).controller('StockOpnamesIndexCtrl', ['$scope', 'storeStockOpnames', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeStockOpnames, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset(),
            'with': '["user"]'
        };
        // Filtering
        var filter = [];
        if ($scope.query) {
            filter.push({
                field: 'name',
                operator: 'like',
                value: $scope.query
            });
        }
        // if ([3, 4, 5].indexOf(appAuth.profile.roles[0].id) !== -1) {
        //     filter.push({
        //         field: 'user_id',
        //         operator: '=',
        //         value: appAuth.profile.id
        //     });
        // }
        if (filter.length) params.filter = JSON.stringify(filter);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeStockOpnames.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/stockopnames/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('StockOpnamesAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeStockOpnames', 'stockopnamesDetail', 'appPopup', 'seedbeds', 'storeSeedbeds', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeStockOpnames, stockopnamesDetail, appPopup, seedbeds, storeSeedbeds) {
    var isContinueAdd = false;
    $scope.data = stockopnamesDetail && stockopnamesDetail.data ? stockopnamesDetail.data : {
        // user_id: appAuth.profile.id
    };
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if (!$scope.data.id) {
            $scope.promise = storeStockOpnames.save(angular.extend($scope.data, {
                access_token: appAuth.token,
                seedbed_id: $scope.seedbed.id,
                details: $scope.seedbedDetails.map(function(seedbed){
                    return {
                        id: seedbed.id,
                        new_quantity: seedbed.new_quantity
                    }
                })
            }), function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.seedbed = {};
                    $scope.seedbedDetails = [];
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storeStockOpnames.update({
                id: $scope.data.id
            }, angular.extend($scope.data, {
                access_token: appAuth.token,
            }), function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };
    
    $scope.seedbeds = seedbeds;
    $scope.seedbed = {};
    $scope.seedbedDetails = [];

    $scope.onSeedbedChanged = function(seedbed) {
        $scope.promise = storeSeedbeds.get({
            id: seedbed.id,
            access_token: appAuth.token
        }, function(response) {
            $scope.seedbedDetails = response.data.details
        });
    }
}])
.controller('StockOpnamesDetailCtrl', ['$scope', '$state', 'stockopnamesDetail', function($scope, $state, stockopnamesDetail){
    $scope.seedbed = stockopnamesDetail.data[$state.params.id][0].seedbed;
    $scope.seedbedDetails = stockopnamesDetail.data[$state.params.id].map(function(stockopname){
        return {
            date: stockopname.seedbed_detail.date,
            type: stockopname.seedbed_detail.type,
            quantity: stockopname.old_quantity,
            new_quantity: stockopname.new_quantity,
        };
    });
}]);