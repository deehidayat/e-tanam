'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:SeedsCtrl
 * @description
 * # SeedsCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.seeds', {
        url: '/permohonan-bibit',
        abstract: true,
        controller: 'SeedsCtrl',
        templateUrl: 'views/seeds/main.html'
    }).state('app.seeds.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/seeds/index.html',
                controller: 'SeedsIndexCtrl'
            }
        }
    }).state('app.seeds.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/seeds/form.html',
                controller: 'SeedsAddCtrl'
            }
        },
        resolve: {
            users: ['storeUsers', '$q', 'appAuth', function(storeUsers, $q, appAuth) {
                var defer = $q.defer();
                storeUsers.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            plants: ['storePlants', '$q', 'appAuth', function(storePlants, $q, appAuth) {
                var defer = $q.defer();
                storePlants.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            seedsDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.seeds.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/seeds/form.html',
                controller: 'SeedsAddCtrl'
            }
        },
        resolve: {
            users: ['storeUsers', '$q', 'appAuth', function(storeUsers, $q, appAuth) {
                var defer = $q.defer();
                storeUsers.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            plants: ['storePlants', '$q', 'appAuth', function(storePlants, $q, appAuth) {
                var defer = $q.defer();
                storePlants.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            seedsDetail: ['$stateParams', '$q', 'storeSeeds', 'appAuth', function($stateParams, $q, storeSeeds, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeSeeds.get({
                        id: $stateParams.id,
                        access_token: appAuth.token
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeSeeds', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/seeds/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('SeedsCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Permohonan Bibit',
        route: 'seeds',
        subtitle: ''
    };
}]).controller('SeedsIndexCtrl', ['$scope', 'storeSeeds', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeSeeds, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset(),
            'with': '["user", "plant"]'
        };
        // Filtering
        var filter = [];
        if ($scope.query) {
            filter.push({
                field: 'name',
                operator: 'like',
                value: $scope.query
            });
        }
        if ([3, 4, 5].indexOf(appAuth.profile.roles[0].id) !== -1) {
            filter.push({
                field: 'user_id',
                operator: '=',
                value: appAuth.profile.id
            });
        }
        if (filter.length) params.filter = JSON.stringify(filter);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeSeeds.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/seeds/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('SeedsAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeSeeds', 'users', 'seedsDetail', 'appPopup', 'plants', 'storeLocations', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeSeeds, users, seedsDetail, appPopup, plants, storeLocations) {
    var isContinueAdd = false;
    $scope.data = seedsDetail && seedsDetail.data ? seedsDetail.data : {
        user_id: appAuth.profile.id
    };
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if (!$scope.data.id) {
            $scope.promise = storeSeeds.save(angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id
            }), function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.coord = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storeSeeds.update({
                id: $scope.data.id
            }, angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                location_id: $scope.location ? $scope.location.id : $scope.village.id
            }), function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };
    
    $scope.users = [{
        id: appAuth.profile.id,
        name: appAuth.profile.name
    }];
    $scope.users = $scope.users.concat(users);
    $scope.plants = plants;
    $scope.coord = seedsDetail && seedsDetail.data ? {
        lon: seedsDetail.data.lon,
        lat: seedsDetail.data.lat
    } : {};
    
    // Location
    $scope.location = $scope.data.location ? $scope.data.location : '';
    $scope.removeLocation = function() {
        $scope.location = null;
    }
    var getLocation = function(query, type, offset, limit) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: limit || 20,
            offset: offset || 0,
            filter: []
        };
        // Filtering
        if (query) {
            if (angular.isString(query)) {
                params.filter.push({
                    field: 'name',
                    operator: 'like',
                    value: query
                });
            } else if (angular.isObject(query)) {
                for (var key in query) {
                    params.filter.push({
                        field: key,
                        operator: '=',
                        value: query[key]
                    });
                }
            }
        } 
        if (type === 'city') {
            params.filter.push({
                field: 'province_id',
                operator: '=',
                value: '32'
            });
            params.filter.push({
                field: 'city_id',
                operator: '!=',
                value: ''
            });
            params.filter.push({
                field: 'district_id',
                operator: '=',
                value: ''
            });
        } else if (type === 'district') {
            params.filter.push({
                field: 'district_id',
                operator: '!=',
                value: ''
            });  
            params.filter.push({
                field: 'village_id',
                operator: '=',
                value: ''
            });  
        } else {
            params.filter.push({
                field: 'village_id',
                operator: '!=',
                value: ''
            });  
        }
        if (params.filter.length) params.filter = JSON.stringify(params.filter);
        params.order = JSON.stringify([{
            field: 'name',
            sort: 'asc',
        }]);
        return storeLocations.query(params).$promise.then(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            if (response.data) {
                return response.data;
            }
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    // Kab/Kota
    $scope.city = null;
    $scope.cities = [];
    // Kecamatan
    $scope.district = null;
    $scope.districts = [];
    // Desa
    $scope.village = null;
    $scope.villages = [];
    // Listeners
    getLocation(null, 'city', 0, 1000).then(function(data){
        $scope.cities = data;
    });
    $scope.$watch('city', function(newVal, oldVal) {
        if (newVal) {
            getLocation({ city_id: newVal.city_id }, 'district', 0, 1000).then(function(data){
                $scope.districts = data;
                $scope.district = null;
                $scope.village = null;
            });
        }
    });
    $scope.$watch('district', function(newVal, oldVal) {
        if (newVal) {
            getLocation({ district_id: newVal.district_id }, 'village', 0, 1000).then(function(data){
                $scope.villages = data;
                $scope.village = null;
            });
        }
    });
}]);