'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:RolesCtrl
 * @description
 * # RolesCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.roles', {
        url: '/hak-akses',
        abstract: true,
        controller: 'RolesCtrl',
        templateUrl: 'views/roles/main.html'
    }).state('app.roles.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/roles/index.html',
                controller: 'RolesIndexCtrl'
            }
        }
    }).state('app.roles.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/roles/form.html',
                controller: 'RolesAddCtrl'
            }
        },
        resolve: {
            permissions: ['storePermissions', '$q', 'appAuth', function(storePermissions, $q, appAuth) {
                var defer = $q.defer();
                storePermissions.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            rolesDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.roles.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/roles/form.html',
                controller: 'RolesAddCtrl'
            }
        },
        resolve: {
            permissions: ['storePermissions', '$q', 'appAuth', function(storePermissions, $q, appAuth) {
                var defer = $q.defer();
                storePermissions.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            rolesDetail: ['$stateParams', '$q', 'storeRoles', 'appAuth', function($stateParams, $q, storeRoles, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeRoles.get({
                        id: $stateParams.id,
                        access_token: appAuth.token,
                        'with': '["permissions"]'
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeRoles', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/roles/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).factory('storePermissions', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/permissions/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}).controller('RolesCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Hak Akses',
        route: 'roles',
        subtitle: ''
    };
}]).controller('RolesIndexCtrl', ['$scope', 'storeRoles', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeRoles, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset()
        };
        // Filtering
        if ($scope.query) params.filter = JSON.stringify([{
            "field": "name",
            "operator": "like",
            "value": $scope.query
        }]);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeRoles.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/roles/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('RolesAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeRoles', 'rolesDetail', 'appPopup', 'permissions', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeRoles, rolesDetail, appPopup, permissions) {
    var isContinueAdd = false;
    $scope.data = rolesDetail && rolesDetail.data ? rolesDetail.data : {};
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        angular.element('.tile').addClass('refreshing');
        var permissions = [];
        // if($scope.selectedPermission) {
        //     angular.forEach($scope.selectedPermission, function(value, key){
        //         permissions.push(key);
        //     })
        // }
        if (!$scope.data.id) {
            storeRoles.save({
                access_token: appAuth.token,
                description: $scope.data.description,
                level: $scope.data.level,
                name: $scope.data.name,
                permission_ids: $scope.selectedPermission
            }, function(response, status) {
                angular.element('.tile.refreshing').removeClass('refreshing');
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                angular.element('.tile.refreshing').removeClass('refreshing');
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            storeRoles.update({
                id: $scope.data.id
            }, {
                access_token: appAuth.token,
                description: $scope.data.description,
                level: $scope.data.level,
                name: $scope.data.name,
                permission_ids: $scope.selectedPermission
            }, function(response, status) {
                angular.element('.tile.refreshing').removeClass('refreshing');
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                angular.element('.tile.refreshing').removeClass('refreshing');
                return appPopup.showResponseAPI(response, status);
            });
        }
    };


    $scope.permissions = permissions;

    $scope.limit = 1000, $scope.page = $stateParams.page || 1;
    $scope.selectedPermission = {}, $scope.message = {};

    angular.forEach($scope.data.permissions || [], function(permission) {
        $scope.selectedPermission[permission.id] = true;
    });

    $scope.selectedAll = false;
    $scope.selectAll = function(status) {
        angular.forEach($scope.permissions, function(value, key) {
            $scope.selectedPermission[value.id] = !!status;
        });
    }
}]);