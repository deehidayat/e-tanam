'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp')
  .controller('MainCtrl', ['$scope', '$http', '$window', 'appAuth', '$state', '$location', '$modal', function ($scope, $http, $window, appAuth, $state, $location, $modal) {

    $scope.mainPromise = null;
    $scope.main = {
      title: 'Minovate',
      settings: {
        navbarHeaderColor: 'scheme-cyan',
        sidebarColor: 'scheme-black',
        brandingColor: 'scheme-cyan',
        activeColor: 'black-scheme-color',
        headerFixed: true,
        asideFixed: true,
        rightbarShow: false
      }
    };

    $scope.ajaxFaker = function(){
      $scope.data=[];
      var url = 'http://www.filltext.com/?rows=10&fname={firstName}&lname={lastName}&delay=5&callback=JSON_CALLBACK';

      $http.jsonp(url).success(function(data){
        $scope.data=data;
        console.log('cecky');
        angular.element('.tile.refreshing').removeClass('refreshing');
      });
    };

    $scope.logout = function() {
      appAuth.logout().then(function(){
        $state.go('app.login');
      });
    }

    $scope.historyBack = function() {
      $window.history.back();
    }

    $scope.appAuth = appAuth;
    $scope.$location = $location;

    // Import Data
    $scope.openUpdateMe = function() {
        var popup = $modal.open({
            modal: true,
            size: 'md',
            animation: false,
            templateUrl: 'views/profileForm.html',
            controller: 'MeUpdateCtrl'
        });
        popup.result.then(function(response, status) {
            $scope.mainPromise = appAuth.getMe();
        });
    };
    console.log($scope.appAuth);

  }]).controller('MeUpdateCtrl', ['$scope', 'appAuth', 'appPopup', '$modalInstance', function($scope, appAuth, appPopup, $modalInstance){
    $scope.data = angular.copy(appAuth.profile);
    $scope.data.password = '';
    $scope.confirm_password = '';

    $scope.save = function(isContinue) {
        $scope.promise = appAuth.updateMe({
            access_token: appAuth.token,
            password: $scope.data.password ? $scope.data.password : null,
            name: $scope.data.name
        }).then(function(response, status) {
            $modalInstance.close(true);
            return appPopup.showResponseAPI(response, status);
        }, function(response, status) {
            return appPopup.showResponseAPI(response, status);
        });
    };
  }]);