'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:DistrictsCtrl
 * @description
 * # DistrictsCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.districts', {
        url: '/kecamatan',
        abstract: true,
        controller: 'DistrictsCtrl',
        templateUrl: 'views/districts/main.html'
    }).state('app.districts.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/districts/index.html',
                controller: 'DistrictsIndexCtrl'
            }
        }
    }).state('app.districts.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/districts/form.html',
                controller: 'DistrictsAddCtrl'
            }
        },
        resolve: {
            districtsDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.districts.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/districts/form.html',
                controller: 'DistrictsAddCtrl'
            }
        },
        resolve: {
            districtsDetail: ['$stateParams', '$q', 'storeDistricts', 'appAuth', function($stateParams, $q, storeDistricts, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeDistricts.get({
                        id: $stateParams.id,
                        access_token: appAuth.token
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeDistricts', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/districts/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('DistrictsCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Kecamatan',
        route: 'districts',
        subtitle: ''
    };
}]).controller('DistrictsIndexCtrl', ['$scope', 'storeDistricts', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeDistricts, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset()
        };
        // Filtering
        if ($scope.query) params.filter = JSON.stringify([{
            "field": "name",
            "operator": "like",
            "value": $scope.query
        }]);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeDistricts.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/districts/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('DistrictsAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeDistricts', 'districtsDetail', 'appPopup', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeDistricts, districtsDetail, appPopup) {
    var isContinueAdd = false;
    $scope.data = districtsDetail && districtsDetail.data ? districtsDetail.data : {};
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if (!$scope.data.id) {
            $scope.promise = storeDistricts.save({
                access_token: appAuth.token,
                code: $scope.data.code,
                name: $scope.data.name,
            }, function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storeDistricts.update({
                id: $scope.data.id
            }, {
                access_token: appAuth.token,
                code: $scope.data.code,
                name: $scope.data.name
            }, function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };
}]);