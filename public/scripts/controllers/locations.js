'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:LocationsCtrl
 * @description
 * # LocationsCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('app.locations', {
        url: '/lokasi',
        abstract: true,
        controller: 'LocationsCtrl',
        templateUrl: 'views/locations/main.html'
    }).state('app.locations.index', {
        url: '/index?page',
        views: {
            'page-content': {
                templateUrl: 'views/locations/index.html',
                controller: 'LocationsIndexCtrl'
            }
        }
    }).state('app.locations.add', {
        url: '/tambah?backTo=null',
        views: {
            'page-content': {
                templateUrl: 'views/locations/form.html',
                controller: 'LocationsAddCtrl'
            }
        },
        resolve: {
            users: ['storeUsers', '$q', 'appAuth', function(storeUsers, $q, appAuth) {
                var defer = $q.defer();
                storeUsers.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            locationsDetail: [function() {
                return null;
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    }).state('app.locations.update', {
        url: '/perbarui/:id',
        views: {
            'page-content': {
                templateUrl: 'views/locations/form.html',
                controller: 'LocationsAddCtrl'
            }
        },
        resolve: {
            users: ['storeUsers', '$q', 'appAuth', function(storeUsers, $q, appAuth) {
                var defer = $q.defer();
                storeUsers.query({
                    access_token: appAuth.token
                }).$promise.then(function(response) {
                    defer.resolve(response.data || []);
                }, function(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }],
            locationsDetail: ['$stateParams', '$q', 'storeLocations', 'appAuth', function($stateParams, $q, storeLocations, appAuth) {
                if ($stateParams.id) {
                    var defer = $q.defer();
                    storeLocations.get({
                        id: $stateParams.id,
                        access_token: appAuth.token
                    }, function(a) {
                        defer.resolve(a);
                    });
                    return defer.promise;
                } else {
                    return null;
                }
            }]
        },
        data: {
            accessLevel: AUTHConfig.accessLevels.user
        }
    });
}]).factory('storeLocations', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/locations/:id', {
        id: '@id'
    }, {
        get: {
            method: 'GET',
            cache: false
        },
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
}]).controller('LocationsCtrl', ['$scope', function($scope) {
    $scope.pageInformation = {
        title: 'Deskripsi Tanaman',
        route: 'locations',
        subtitle: ''
    };
}]).controller('LocationsIndexCtrl', ['$scope', 'storeLocations', '$state', '$stateParams', 'appConfig', 'appAuth', 'appPopup', '$filter', '$http', function($scope, storeLocations, $state, $stateParams, appConfig, appAuth, appPopup, $filter, $http) {
    $scope.limit = 15, $scope.page = $stateParams.page || 1;
    $scope.data = [], $scope.message = {};
    $scope.getOffset = function() {
        return ($scope.page - 1) * $scope.limit;
    }
    $scope.paging = function() {
        $state.transitionTo('app.' + $scope.pageInformation.route + '.index', {
            page: $scope.page
        });
    };
    $scope.query = '';
    $scope.getData = function(tableState) {
        angular.element('.tile').addClass('refreshing');
        var params = {
            access_token: appAuth.token,
            limit: $scope.limit,
            offset: $scope.getOffset()
        };
        // Filtering
        if ($scope.query) params.filter = JSON.stringify([{
            field: 'number',
            operator: 'like',
            value: $scope.query
        }]);
        // Sorting
        if (tableState && tableState.sort.predicate) {
            params.order = JSON.stringify([{
                field: tableState.sort.predicate,
                sort: tableState.sort.reverse ? 'desc' : 'asc',
            }]);
        }
        storeLocations.query(params).$promise.then(function(response, status) {
            if (response.data) {
                $scope.data = response.data;
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    $scope.selectAll = function(status) {
        angular.forEach($scope.data, function(value, key) {
            value.selected = !!status;
        });
    }
    $scope.deleteRecord = function(item) {
        angular.element('.tile').addClass('refreshing');
        // DELETE MENGGUNAKAN $http, karena $resource tidak mau mengirim body
        var config = {
            method: 'DELETE',
            url: appConfig.api.get('baseUrl') + '/locations/' + item.id,
            params: {
                access_token: appAuth.token
            },
            data: {
                access_token: appAuth.token
            }
        };
        return $http(config).success(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            $scope.getData();
            return appPopup.showResponseAPI(response, status);
        }).error(function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            return appPopup.showResponseAPI(response, status);
        });
    }
    $scope.bulkActions = appAuth.authenticated ? [{
        name: 'Delete All',
        fn: function() {
            var data = $filter('filter')($scope.data, function(value, key) {
                return value.selected === true;
            });
            if (data.length) {
                var deleteRecrusive = function (items) {
                    if (!items || !items.length) return;
                    var item = items[0];
                    $scope.deleteRecord(item).success(function(response, status) {
                        if (status === 200) {
                            items.splice(0, 1); // remove item yang sudah dihapus
                            deleteRecrusive(items);
                        }
                    });
                }
                return deleteRecrusive(data);
            }
            return appPopup.toast('No item selected', 'Warning', {
                iconType: 'fa-warning',
                iconClass: 'bg-warning'
            });
        }
    }] : null;
    $scope.$watch('query', function(newVal, oldVal) {
        if (!angular.equals(newVal, oldVal)) {
            $scope.getData();
        }
    });
    $scope.getData();
}]).controller('LocationsAddCtrl', ['$scope', 'appConfig', '$q', 'appAuth', '$state', '$stateParams', 'storeLocations', 'users', 'locationsDetail', 'appPopup', 'Upload', function($scope, appConfig, $q, appAuth, $state, $stateParams, storeLocations, users, locationsDetail, appPopup, Upload) {
    var isContinueAdd = false;
    $scope.data = locationsDetail && locationsDetail.data ? locationsDetail.data : {};
    $scope.save = function(isContinue) {
        isContinueAdd = !!isContinue;
        if ($scope.files.length) {
            return doUploadAll();
        }
        if (!$scope.data.id) {
            $scope.promise = storeLocations.save(angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat
            }), function(response, status) {
                if (isContinueAdd) {
                    $scope.data = {};
                    $scope.coord = {};
                    $scope.form.$setPristine();
                } else {
                    if ($state.params.backTo) {
                        $state.go($state.params.backTo)
                    } else {
                        $state.go('app.' + $scope.pageInformation.route + '.index')
                    }
                }
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        } else {
            $scope.promise = storeLocations.update({
                id: $scope.data.id
            }, angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat,
                oldPhotos: $scope.oldPhotos.map(function(item){
                    return {
                        id: item.id,
                        caption: item.caption,
                        deleted: item.deleted
                    }
                })
            }), function(response, status) {
                $state.go('app.' + $scope.pageInformation.route + '.index');
                return appPopup.showResponseAPI(response, status);
            }, function(response, status) {
                return appPopup.showResponseAPI(response, status);
            });
        }
    };
    
    $scope.users = users;
    $scope.coord = locationsDetail && locationsDetail.data ? {
        lon: locationsDetail.data.lon,
        lat: locationsDetail.data.lat
    } : {};

    // Uploader
    $scope.oldPhotos = $scope.data.photos;
    delete $scope.data.photos;
    $scope.files = [];
    $scope.removeFromQueue = function(index) {
        $scope.files.splice(index, 1);
    };
    $scope.onFileSelect = function(files) {
        files && files.forEach && files.forEach(function(file, key) {
            $scope.files.push({
                id: key,
                file: file
            });
        });
    };
    function doUploadAll(isContinue) {
        Upload.upload({
            url: $scope.data.id ? '/api/v1/locations/' + $scope.data.id : '/api/v1/locations',
            method: $scope.data.id ? 'POST' : 'POST',
            data: angular.extend($scope.data, {
                access_token: appAuth.token,
                lon: $scope.coord.lon,
                lat: $scope.coord.lat
            }, {
                photos: $scope.files.map(function(item) {
                    return item.file;
                }),
                captions: $scope.files.map(function(item) {
                    return item.caption || item.file.name;
                })
            })
        }).then(function(response) {
            if (isContinueAdd) {
                $scope.data = {};
                $scope.coord = {};
                $scope.form.$setPristine();
                $scope.files = [];
            } else {
                if ($state.params.backTo) {
                    $state.go($state.params.backTo)
                } else {
                    $state.go('app.' + $scope.pageInformation.route + '.index')
                }
            }
            return appPopup.showResponseAPI(response, response.status);
        }, function(response) {
            return appPopup.showResponseAPI(response, response.status);
        }, function(evt) {
            $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    }
    $scope.clearExt = function(val) {
        return val.replace(/\.([a-zA-Z]{3,})/,'');
    }
}]);