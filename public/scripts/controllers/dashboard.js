'use strict';
/**
 * @ngdoc function
 * @name minovateApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the minovateApp
 */
angular.module('minovateApp').factory('storeDashboards', ['$resource', 'appConfig', function($resource, appConfig) {
    return $resource(appConfig.api.get('baseUrl') + '/dashboards/:id', {
        id: '@id'
    }, {
        query: {
            method: 'GET',
            cache: false,
            isArray: false
        }
    });
}]).controller('DashboardCtrl', ['$scope', '$http', '$moment', 'olData', 'olHelpers', '$location', '$timeout', 'appPopup', 'storeDashboards', '$state', '$modal', '$filter', function($scope, $http, $moment, olData, olHelpers, $location, $timeout, appPopup, storeDashboards, $state, $modal, $filter) {
    /**
     * Date Range
     */
    // $scope.startDate = $moment().subtract(29, 'days').format('MMMM D, YYYY');
    // $scope.endDate = $moment().format('MMMM D, YYYY');
    $scope.startDate = $moment().startOf('year');
    $scope.endDate = $moment().endOf('year');
    $scope.rangeOptions = {
        ranges: {
            // 'Hari Ini': [$moment(), $moment()],
            // 'Kemarin': [$moment().subtract(1, 'days'), $moment().subtract(1, 'days')],
            // 'Seminggu Terakhir': [$moment().subtract(6, 'days'), $moment()],
            '30 Hari Terakhir': [$moment().subtract(29, 'days'), $moment()],
            'Bulan Ini': [$moment().startOf('month'), $moment().endOf('month')],
            'Bulan Sebelumnya': [$moment().subtract(1, 'month').startOf('month'), $moment().subtract(1, 'month').endOf('month')],
            'Tahun Ini': [$moment().startOf('year'), $moment().endOf('year')],
            'Tahun Sebelumnya': [$moment().subtract(1, 'year').startOf('year'), $moment().subtract(1, 'year').endOf('year')],
        },
        opens: 'left',
        startDate: angular.copy($scope.startDate),
        endDate: angular.copy($scope.endDate),
        parentEl: '#date-range-container'
    };
    /**
     * PETA
     */
    $scope.mapConfig = {};
    // var batasKecLayer = {
    //     name: 'base_kecamatan',
    //     label: 'Batas Kecamatan',
    //     group: 'layers',
    //     source: {
    //         type: 'GeoJSON',
    //         url: 'layers/batas_kec.geojson'
    //     },
    //     opacity: 0.5,
    //     style: {
    //         fill: {
    //             color: '#FFFFFF'
    //         },
    //         stroke: {
    //             color: 'black',
    //             width: 0
    //         }
    //     }
    // };
    $scope.customMarker = {
        image: {
            icon: {
                anchor: [0.5, 1],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                opacity: 0.90,
                src: 'images/marker.png'
            }
        }
    }; 
    
    var updateMapData = function() {
        $scope.mapConfig.points = [];
        storeDashboards.query({
            id: 'plantings',
            plain: true,
            filter: JSON.stringify([{
                field: 'planting_date',
                operator: 'between',
                value: [$moment($scope.startDate).format('YYYY-M-D'), $moment($scope.endDate).format('YYYY-M-D')]
            }])
        }).$promise.then(function(response, status) {
            if (response.data) {
                for (var idx in response.data) {
                    if (response.data[idx].lat && response.data[idx].lon) {
                        $scope.mapConfig.points.push({
                            data: response.data[idx],
                            projection: 'EPSG:4326',
                            lat: response.data[idx].lat,
                            lon: response.data[idx].lon,
                            label: {
                                message: 
                                    'Lokasi Penanaman ' +
                                    '<br>Tahun: ' + response.data[idx].year + 
                                    '<br>Jenis: ' + response.data[idx].emission.name + 
                                    '<br>Jumlah: ' + $filter('number')(response.data[idx].quantity) + ' Pohon' +
                                    '<br>Persentase Tumbuh: ' + $filter('number')(response.data[idx].grow_percent) + ' %' +
                                    '<br>Capaian Penurunan Emisi: ' + $filter('number')(response.data[idx].calculation) + ' Ton Co<sup>2</sup> Eq'
                                ,
                                show: false,
                                showOnMouseOver: true
                            }
                        });
                    }
                }
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
        storeDashboards.query({
            id: 'seedbeds'
            // filter: JSON.stringify([{
            //     field: 'planting_date',
            //     operator: 'between',
            //     value: [$moment($scope.startDate).format('YYYY-M-D'), $moment($scope.endDate).format('YYYY-M-D')]
            // }])
        }).$promise.then(function(response, status) {
            if (response.data) {
                for (var idx in response.data) {
                    if (response.data[idx].lat && response.data[idx].lon) {
                        $scope.mapConfig.points.push({
                            data: response.data[idx],
                            projection: 'EPSG:4326',
                            lat: response.data[idx].lat,
                            lon: response.data[idx].lon,
                            label: {
                                message: 
                                    'Lokasi Persemaian ' +
                                    '<br>Alamat: ' + response.data[idx].address + 
                                    '<br>Luas: ' + $filter('number')(response.data[idx].wide) + ' Ha<sup>2</sup>'
                                ,
                                show: false,
                                showOnMouseOver: true
                            },
                            style: $scope.customMarker
                        });
                    }
                }
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
    var mapColors = ["#ea385e", "#b75331", "#a32d20", "#aa1953", "#b2481e", "#c10b5a", "#bf394d", "#ffd1c9", "#e575a0", "#cc483f", "#d88665", "#c92a44", "#e27d76", "#d34768", "#e0455c", "#e24660", "#f4386d", "#f9a4c5", "#f48f64", "#ea8e8c", "#f4a086", "#f73169", "#ef715b", "#f2abb3", "#f2a9c1", "#f98eb9", "#edb19e", "#ea6085", "#e58e97", "#ef856e", "#ef77a9", "#eaa293", "#f990ab", "#ea9faf", "#fc9f8a", "#fcb8c0", "#fcc8b8", "#ff895e", "#ffb2b8"];
    // Inline GeoJSON
    var sourceTemplate = {
        type: 'GeoJSON',
        geojson: {
            projection: 'EPSG:3857',
            object: {}
        }
    };
    angular.extend($scope.mapConfig, {
        center: {
            lat: -6.970049417296224,
            lon: 107.79235839843751,
            zoom: 8.5,
            bounds: [],
            centerUrlHash: false
        },
        defaults: {
            view: {
                maxZoom: 12,
                minZoom: 3,
                rotation: 90
            },
            layers: {
                main: {
                    source: {
                        type: 'OSM',
                        url: 'http://{a-c}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png'
                    }
                }
            },
            interactions: {
                mouseWheelZoom: false
            },
            controls: {
                zoom: true,
                rotate: true,
                attribution: false
            },
            controls: {
                zoom: {
                    position: 'topright'
                }
            },
            events: {
                map: ['singleclick', 'pointermove'],
                layers: ['mousemove', 'click']
            }
        },
        // Layer yang akan ditampilkan
        layers: {
            // mapbox: {
            //     name: 'Mapbox',
            //     active: true,
            //     opacity: 0.5,
            //     source: {
            //         type: 'TileJSON',
            //         url: 'http://api.tiles.mapbox.com/v3/mapbox.geography-class.jsonp'
            //     }
            // },
            abing: {
                name: 'Bing',
                active: true,
                opacity: 1,
                source: {
                    type: 'BingMaps',
                    key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
                    imagerySet: 'Road' // Aerial
                }
            },
            // KML
            // base_kecamatan: {
            //     name: 'batas_kec',
            //     source: {
            //         type: 'KML',
            //         projection: 'EPSG:3857',
            //         url: 'layers/batas_kec.kml'
            //     },
            //     style: {
            //         fill: {
            //             color: '#5f2c2c'
            //         },
            //         stroke: {
            //             color: '#FFFFFF',
            //             width: 3
            //         }
            //     }
            // },
            // GeoJSON
            // base_kecamatan: angular.copy(batasKecLayer)
        },
        points: [],
        // heatmap: {
        //     type: 'Heatmap',
        //     source: {
        //         type: 'KML',
        //         projection: 'EPSG:3857',
        //         url: 'layers/earthquakes.kml',
        //         radius: 5
        //     }
        // },
        // // Marker
        // eiffel: {
        //     lat: -7.458093,
        //     lon: 108.314694,
        //     label: {
        //         message: '<img src="images/logo-placeholder.jpg" />',
        //         show: false,
        //         showOnMouseOver: true
        //     }
        // },
        // notredame: {
        //     lat: 48.852966,
        //     lon: 2.349902,
        //     label: {
        //         message: '<img src="images/random-avatar8.jpg" />',
        //         show: false,
        //         showOnMouseOver: true
        //     }
        // },
        // versailles: {
        //     lat: 48.804722,
        //     lon: 2.121782,
        //     label: {
        //         message: '<img src="images/random-avatar1.jpg" />',
        //         show: false,
        //         showOnMouseOver: true
        //     }
        // },
        mouseposition: {
            lat: -7.5095,
            lon: 108.4213,
            projection: 'EPSG:4326'
        },
        mouseclickposition: {},
        projection: 'EPSG:4326',
        centerAndShow: function(id) {
            if (!$scope[id].label.show) {
                $scope.center.lat = $scope[id].lat;
                $scope.center.lon = $scope[id].lon;
            }
            $scope[id].label.show = !$scope[id].label.show
        }
    });
    $scope.onPointClick = function(point) {
        if (!point.data.photos || !point.data.photos.length) {
            return
        }
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            size: 'md',
            modal: true,
            controller: ['$scope', 'photos', function($scope, photos) {
                $scope.photos = photos;
            }],
            resolve: {
                photos: function() {
                    return point.data.photos;
                }
            }
        });
        modalInstance.result.then(function(selectedItem) {}, function() {});
        // $state.go('app.plantings.update', {id: point.data.id});
    }
    $scope.$on('openlayers.map.pointermove', function(event, data) {
        $scope.$apply(function() {
            if ($scope.mapConfig.projection === data.projection) {
                $scope.mapConfig.mouseposition = data.coord;
            } else {
                var p = ol.proj.transform([data.coord[0], data.coord[1]], data.projection, $scope.mapConfig.projection);
                $scope.mapConfig.mouseposition = {
                    lat: p[1],
                    lon: p[0],
                    projection: $scope.mapConfig.projection
                }
            }
        });
    });
    $scope.$on('openlayers.map.singleclick', function(event, data) {
        $scope.$apply(function() {
            if ($scope.mapConfig.projection === data.projection) {
                $scope.mapConfig.mouseclickposition = data.coord;
            } else {
                var p = ol.proj.transform([data.coord[0], data.coord[1]], data.projection, $scope.mapConfig.projection);
                $scope.mapConfig.mouseclickposition = {
                    lat: p[1],
                    lon: p[0],
                    projection: $scope.mapConfig.projection
                }
            }
        });
    });
    $scope.$on("centerUrlHash", function(event, centerHash) {
        $location.search({
            c: centerHash
        });
    });
    // $scope.$on('openlayers.layers.base_kecamatan.mousemove', function(event, feature) {
    //     // console.info(event, feature);
    //     // $scope.$apply(function(scope) {
    //     // if(feature && $scope.countries[feature.getId()]) {
    //     //     $scope.mouseMoveCountry = feature?$scope.countries[feature.getId()].name:'';
    //     // }
    //     // });
    //     // feature.setStyle(olHelpers.createStyle({
    //     //     fill: {
    //     //         color: 'rgba(255, 0, 0, 0.2)'
    //     //     }
    //     // }));
    // });
    // $scope.$on('openlayers.layers.base_kecamatan.click', function(event, feature) {
    //     console.info(event, feature);
    //     // $scope.$apply(function(scope) {
    //     // if(feature) {
    //     //     $scope.mouseClickCountry = feature?$scope.countries[feature.getId()].name:'';
    //     // }
    //     // });
    // });
    // olData.getMap().then(function(map) {
    //     var previousFeature;
    //     var overlay = new ol.Overlay({
    //         element: document.getElementById('countrybox'),
    //         positioning: 'center-center',
    //         offset: [-25, 59],
    //         position: [0, 0]
    //     });
    //     var overlayHidden = true;
    //     // Mouse over function, called from the Leaflet Map Events
    //     $scope.$on('openlayers.layers.base_kecamatan.mousemove', function(event, feature, olEvent) {
    //         $scope.$apply(function(scope) {
    //             scope.selectedCountry = feature ? $scope.countries[feature.getId()] : '';
    //         });
    //         console.log(feature);
    //         if (!feature) {
    //             map.removeOverlay(overlay);
    //             overlayHidden = true;
    //             return;
    //         } else if (overlayHidden) {
    //             map.addOverlay(overlay);
    //             overlayHidden = false;
    //         }
    //         overlay.setPosition(map.getEventCoordinate(olEvent));
    //         if (feature) {
    //             feature.setStyle(olHelpers.createStyle({
    //                 fill: {
    //                     color: '#FFF'
    //                 }
    //             }));
    //             if (previousFeature && feature !== previousFeature) {
    //                 previousFeature.setStyle(getStyle(previousFeature));
    //             }
    //             previousFeature = feature;
    //         }
    //     });
    // });
    /**
     * Flot Chart
     */
    // $scope.dataset = [
    //     {
    //             data: [
    //                 [1, 15],
    //                 [2, 40],
    //                 [3, 35],
    //                 [4, 39],
    //                 [5, 42],
    //                 [6, 50],
    //                 [7, 46],
    //                 [8, 49],
    //                 [9, 59],
    //                 [10, 60],
    //                 [11, 58],
    //                 [12, 74]
    //             ],
    //             label: 'Unique Visits',
    //             points: {
    //                 show: true,
    //                 radius: 4
    //             },
    //             splines: {
    //                 show: true,
    //                 tension: 0.45,
    //                 lineWidth: 4,
    //                 fill: 0
    //             }
    //         }, 
    //     {
    //         data: [
    //             [1, 0],
    //             [2, 0],
    //             [3, 0],
    //             [4, 0],
    //             [5, 0],
    //             [6, 0],
    //             [7, 0],
    //             [8, 0],
    //             [9, 0],
    //             [10, 0],
    //             [11, 0],
    //             [12, 0]
    //         ],
    //         label: 'Jumlah Bencana',
    //         bars: {
    //             show: true,
    //             barWidth: 0.6,
    //             lineWidth: 0,
    //             fillColor: {
    //                 colors: [{
    //                     opacity: 0.3
    //                 }, {
    //                     opacity: 0.8
    //                 }]
    //             }
    //         }
    //     }
    // ];
    // $scope.lossEstimation = 0;
    // var updateFlotData = function() {
    // };
    // $scope.options = {
    //     colors: ['#61c8b8', '#e05d6f'],
    //     series: {
    //         shadowSize: 0
    //     },
    //     legend: {
    //         backgroundOpacity: 0,
    //         margin: -7,
    //         position: 'ne',
    //         noColumns: 2
    //     },
    //     xaxis: {
    //         tickLength: 0,
    //         font: {
    //             color: '#fff'
    //         },
    //         position: 'bottom',
    //         ticks: [
    //             [1, 'JAN'],
    //             [2, 'FEB'],
    //             [3, 'MAR'],
    //             [4, 'APR'],
    //             [5, 'MAY'],
    //             [6, 'JUN'],
    //             [7, 'JUL'],
    //             [8, 'AUG'],
    //             [9, 'SEP'],
    //             [10, 'OCT'],
    //             [11, 'NOV'],
    //             [12, 'DEC']
    //         ]
    //     },
    //     yaxis: {
    //         tickLength: 0,
    //         font: {
    //             color: '#fff'
    //         }
    //     },
    //     grid: {
    //         borderWidth: {
    //             top: 0,
    //             right: 0,
    //             bottom: 1,
    //             left: 1
    //         },
    //         borderColor: 'rgba(255,255,255,.3)',
    //         margin: 0,
    //         minBorderMargin: 0,
    //         labelMargin: 20,
    //         hoverable: true,
    //         clickable: true,
    //         mouseActiveRadius: 6
    //     },
    //     tooltip: true,
    //     tooltipOpts: {
    //         content: '%s: %y',
    //         defaultTheme: false,
    //         shifts: {
    //             x: 0,
    //             y: 20
    //         }
    //     }
    // };
    /**
     * Statistik
     */
    $scope.statisticData = [];
    var updateStatistic = function() {
        $scope.statisticData = [];
        storeDashboards.query({
            id: 'statistics',
            start_date: $moment($scope.startDate).format('YYYY-M-D'),
            end_date: $moment($scope.endDate).format('YYYY-M-D')
            // filter: JSON.stringify([{
            //     field: 'planting_date',
            //     operator: 'between',
            //     value: [$moment($scope.startDate).format('YYYY-M-D'), $moment($scope.endDate).format('YYYY-M-D')]
            // }])
        }).$promise.then(function(response, status) {
            if (response.data) {
                var idx = 0;
                $scope.statisticData = [];
                angular.forEach(response.data, function(data) {
                    $scope.statisticData.push({
                        label: data.label,
                        value: data.value
                    });
                });
                $scope.statisticData.sort(function(a, b) {
                    return a > b;
                });
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };	
	
    /**
     * Summary
     */
    $scope.bgColors = ['bg-cyan', 'bg-amethyst', 'bg-orange', 'bg-red', 'bg-greensea', 'bg-hotpink', 'bg-dutch', 'bg-drank', 'bg-blue', 'bg-lighter', 'bg-primary', 'bg-success', 'bg-warning', 'bg-danger', 'bg-info', 'bg-default']
    $scope.summaryData = [];
    var updateSummary = function() {
        $scope.summaryData = [];
        storeDashboards.query({
            id: 'summary',
            start_date: $moment($scope.startDate).format('YYYY-M-D'),
            end_date: $moment($scope.endDate).format('YYYY-M-D'),
			grafik:1
        }).$promise.then(function(response, status) {
            if (response.data) {
                $scope.summaryData = response.data;
                // angular.forEach(response.data, function(data) {
                //     $scope.summaryData.push({
                //         label: data.label,
                //         value: data.quantity,
                //         color: colors[idx++]
                //     });
                // });
                // $scope.summaryData.sort(function(a, b) {
                //     return a > b;
                // });
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };

	$scope.bgColors = ['bg-cyan', 'bg-amethyst', 'bg-orange', 'bg-red', 'bg-greensea', 'bg-hotpink', 'bg-dutch', 'bg-drank', 'bg-blue', 'bg-lighter', 'bg-primary', 'bg-success', 'bg-warning', 'bg-danger', 'bg-info', 'bg-default']
    $scope.summaryData2 = [];
    var updateSummary2 = function() {
        $scope.summaryData2 = [];
        storeDashboards.query({
            id: 'summary',
            start_date: $moment($scope.startDate).format('YYYY-M-D'),
            end_date: $moment($scope.endDate).format('YYYY-M-D'),
			grafik:2
        }).$promise.then(function(response, status) {
            if (response.data) {
                $scope.summaryData2 = response.data;
                // angular.forEach(response.data, function(data) {
                //     $scope.summaryData.push({
                //         label: data.label,
                //         value: data.quantity,
                //         color: colors[idx++]
                //     });
                // });
                // $scope.summaryData.sort(function(a, b) {
                //     return a > b;
                // });
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };		
	
	$scope.bgColors = ['bg-cyan', 'bg-amethyst', 'bg-orange', 'bg-red', 'bg-greensea', 'bg-hotpink', 'bg-dutch', 'bg-drank', 'bg-blue', 'bg-lighter', 'bg-primary', 'bg-success', 'bg-warning', 'bg-danger', 'bg-info', 'bg-default']
    $scope.summaryData3 = [];
    var updateSummary3 = function() {
        $scope.summaryData3 = [];
        storeDashboards.query({
            id: 'summary',
            start_date: $moment($scope.startDate).format('YYYY-M-D'),
            end_date: $moment($scope.endDate).format('YYYY-M-D'),
			grafik:3
        }).$promise.then(function(response, status) {
            if (response.data) {
                $scope.summaryData3 = response.data;
                // angular.forEach(response.data, function(data) {
                //     $scope.summaryData.push({
                //         label: data.label,
                //         value: data.quantity,
                //         color: colors[idx++]
                //     });
                // });
                // $scope.summaryData.sort(function(a, b) {
                //     return a > b;
                // });
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };	
	
	$scope.summaryData4 = [];
    var updateSummary4 = function() {
        $scope.summaryData4 = [];
        storeDashboards.query({
            id: 'summary',
            start_date: $moment($scope.startDate).format('YYYY-M-D'),
            end_date: $moment($scope.endDate).format('YYYY-M-D'),
			grafik:4
        }).$promise.then(function(response, status) {
            if (response.data) {
                $scope.summaryData4 = response.data;
                // angular.forEach(response.data, function(data) {
                //     $scope.summaryData.push({
                //         label: data.label,
                //         value: data.quantity,
                //         color: colors[idx++]
                //     });
                // });
                // $scope.summaryData.sort(function(a, b) {
                //     return a > b;
                // });
                $scope.message = response.meta;
            }
            angular.element('.tile.refreshing').removeClass('refreshing');
        }, function(response, status) {
            angular.element('.tile.refreshing').removeClass('refreshing');
            appPopup.showResponseAPI(response, status);
        });
    };
		
    $scope.$watchGroup(['startDate', 'endDate'], function(newVal, oldVal) {
        $scope.refreshDashboard();
    });
    $scope.refreshDashboard = function() {
        updateMapData();
        updateStatistic();
        updateSummary();
		updateSummary2();
		updateSummary3();
		updateSummary4();
        // updateFlotData();
        // updateImpactData();
    };
}]);