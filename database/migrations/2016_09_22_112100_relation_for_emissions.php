<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelationForEmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->integer('emission_id')->unsigned()->after('lat')->nullable();
            $table->foreign('emission_id')->references('id')->on('emissions')
                  ->onUpdate('no action')->onDelete('set null');
        });
        Schema::table('plantings', function (Blueprint $table) {
            $table->integer('emission_id')->unsigned()->after('lat')->nullable();
            $table->foreign('emission_id')->references('id')->on('emissions')
                  ->onUpdate('no action')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plantings', function (Blueprint $table) {
            $table->dropForeign('plantings_emission_id_foreign');
            $table->dropColumn('emission_id');
        });
        Schema::table('plants', function (Blueprint $table) {
            $table->dropForeign('plants_location_id_foreign');
            $table->dropColumn('emission_id');
        });
    }
}
