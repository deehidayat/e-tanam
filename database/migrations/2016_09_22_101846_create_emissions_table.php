<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emissions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->float('emission');
            $table->enum('type', ['Fast Growing', 'Slow Growing', 'Non Fast dan Slow Growing']);
            $table->nullableTimestamps();
        });
        Schema::drop('slow_growing_emissions');
        Schema::drop('fast_growing_emissions');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('fast_growing_emissions', function ($t) {
            $t->increments('id')->unsigned();
            $t->text('name');
            $t->float('emission');
            $t->nullableTimestamps();
        });
        Schema::create('slow_growing_emissions', function ($t) {
            $t->increments('id')->unsigned();
            $t->text('name');
            $t->float('emission');
            $t->nullableTimestamps();
        });
        Schema::drop('emissions');
    }
}
