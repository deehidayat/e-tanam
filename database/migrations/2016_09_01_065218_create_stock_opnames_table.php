<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockOpnamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_opnames', function (Blueprint $table) {
            $table->increments('id');

            $table->string('number')->index();

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->integer('seedbed_id')->unsigned()->index();
            $table->foreign('seedbed_id')->references('id')->on('seedbeds')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->integer('seedbed_detail_id')->unsigned()->index();
            $table->foreign('seedbed_detail_id')->references('id')->on('seedbed_details')
                  ->onUpdate('cascade')->onDelete('cascade');

            $table->integer('old_quantity');
            $table->integer('new_quantity');
            $table->integer('margin');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_opnames');
    }
}
