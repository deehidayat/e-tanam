<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeedbedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('seedbeds', function ($t) {
            $t->increments('id')->unsigned();
            $t->text('address');
            $t->float('lon');
            $t->float('lat');
            $t->float('wide');
            $t->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seedbeds');
    }
}
