<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationIdRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->integer('location_id')->unsigned()->after('lat')->nullable();
            $table->foreign('location_id')->references('id')->on('locations')
                  ->onUpdate('no action')->onDelete('set null');
        });
        Schema::table('plantings', function (Blueprint $table) {
            $table->integer('location_id')->unsigned()->after('lat')->nullable();
            $table->foreign('location_id')->references('id')->on('locations')
                  ->onUpdate('no action')->onDelete('set null');
        });
        Schema::table('seeds', function (Blueprint $table) {
            $table->integer('location_id')->unsigned()->after('lat')->nullable();
            $table->foreign('location_id')->references('id')->on('locations')
                  ->onUpdate('no action')->onDelete('set null');
        });
        Schema::table('seedbeds', function (Blueprint $table) {
            $table->integer('location_id')->unsigned()->after('lat')->nullable();
            $table->foreign('location_id')->references('id')->on('locations')
                  ->onUpdate('no action')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plants', function (Blueprint $table) {
            $table->dropForeign('plants_location_id_foreign');
            $table->dropColumn('location_id');
        });
        Schema::table('plantings', function (Blueprint $table) {
            $table->dropForeign('plantings_location_id_foreign');
            $table->dropColumn('location_id');
        });
        Schema::table('seeds', function (Blueprint $table) {
            $table->dropForeign('seeds_location_id_foreign');
            $table->dropColumn('location_id');
        });
        Schema::table('seedbeds', function (Blueprint $table) {
            $table->dropForeign('seedbeds_location_id_foreign');
            $table->dropColumn('location_id');
        });
    }
}
