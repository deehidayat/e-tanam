<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeedbedIdToSeedbedDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seedbed_details', function (Blueprint $table) {
            $table->integer('seedbed_id')->unsigned()->after('date');
            $table->foreign('seedbed_id')->references('id')->on('seedbeds')
                  ->onUpdate('no action')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seedbed_details', function (Blueprint $table) {
            $table->dropColumn('seedbed_id');
        });
    }
}
