<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSeeds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //		
		Schema::table('plantings', function (Blueprint $table) {
            $table->String('sumber_dana');
        });
		
		Schema::table('seeds', function (Blueprint $table) {
            $table->integer('location_id')->unsigned()->after('lat')->nullable();
            $table->foreign('location_id')->references('id')->on('locations')
                  ->onUpdate('no action')->onDelete('set null');
        });
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //		
		Schema::drop('seeds');
    }
}
