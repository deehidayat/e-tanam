<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plantings', function ($t) {
            $t->increments('id')->unsigned();
            $t->date('planting_date');
            $t->smallInteger('year');
            $t->text('address');
            $t->string('type');
            $t->integer('quantity');
            $t->float('grow_percent');
            $t->float('diameter');
            $t->float('lon', 10, 5);
            $t->float('lat', 10, 5);
            $t->integer('user_id')->unsigned();
            $t->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plantings');
    }
}
