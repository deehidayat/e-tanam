<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

use App\User;
use App\Models\PermissionRole;

use Illuminate\Support\Facades\Route;

class DatabaseSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();
        
        // Init Seeder
        $this->call('UserTableSeeder');
        $this->call('OauthClientSeeder');
        $this->call('RoleSeeder');
        $this->call('PermissionSeeder');
        $this->call('PermissionRoleSeeder');
        
        // Master Data Seeder
    }
}

class UserTableSeeder extends Seeder
{
    
    public function run() {
        Model::unguard();
        $user = User::find(0);
        if (empty($user)) {
            $user = User::create(['id' => 0, 'name' => 'Root', 'email' => 'deehieday@gmail.com', 'password' => Hash::make('root')]);
            $user->id = 0;
            $user->save();
        } else {
            $user->update(['id' => 0, 'name' => 'Root', 'email' => 'ebsakun24@gmail.com', 'password' => Hash::make('root')]);
            $user->save();
        }

        $user = User::find(1);
        if (empty($user)) {
            $user = User::create(['id' => 1, 'name' => 'Super Admin', 'email' => 'super@e-tanam.go.id', 'password' => Hash::make('super')]);
            $user->id = 1;
            $user->save();
        } else {
            $user->update(['id' => 1, 'name' => 'Super Admin', 'email' => 'super@e-tanam.go.id', 'password' => Hash::make('super')]);
            $user->save();
        }

        $user = User::find(2);
        if (empty($user)) {
            $user = User::create(['id' => 2, 'name' => 'Admin', 'email' => 'admin@e-tanam.go.id', 'password' => Hash::make('admin')]);
            $user->id = 2;
            $user->save();
        } else {
            $user->update(['id' => 2, 'name' => 'Admin', 'email' => 'admin@e-tanam.go.id', 'password' => Hash::make('admin')]);
            $user->save();
        }
    }
}

class RoleSeeder extends Seeder
{
    
    public function run() {
        Model::unguard();
        // DB::statement("SET foreign_key_checks = 0");
        
        /*
         ** Define role
        */
        $rootRole = Role::find(0);
        if (empty($rootRole)) {
            $rootRole = Role::create(['id' => 0, 'name' => 'Root', 'slug' => 'root', 'description' => 'Root Developer', 'level' => 0]);
            $rootRole->id = 0;
            $rootRole->save();
        } else {
            $rootRole->update(['id' => 0, 'name' => 'Root', 'slug' => 'root', 'description' => 'Root Developer', 'level' => 0]);
            $rootRole->save();
        }

        $superAdminRole = Role::find(1);
        if (empty($superAdminRole)) {
            $superAdminRole = Role::create(['id' => 1, 'name' => 'Super Admin', 'slug' => 'super_admin', 'description' => 'Bisa merubah behaviour aplikasi, use carefully !!', 'level' => 1]);
            $superAdminRole->id = 1;
            $superAdminRole->save();
        } else {
            $superAdminRole->update(['id' => 1, 'name' => 'Super Admin', 'slug' => 'super_admin', 'description' => 'Bisa merubah behaviour aplikasi, use carefully !!', 'level' => 1]);
            $superAdminRole->save();
        }
        
        $adminRole = Role::find(2);
        if (empty($adminRole)) {
            $adminRole = Role::create(['id' => 2, 'name' => 'Admin', 'slug' => 'admin', 'description' => 'Bisa menambah atau menghapus user lain', 'level' => 2]);
            $adminRole->id = 2;
            $adminRole->save();
        } else {
            $adminRole->update(['id' => 2, 'name' => 'Admin', 'slug' => 'admin', 'description' => 'Bisa menambah atau menghapus user lain', 'level' => 2]);
            $adminRole->save();
        }
        
        $bitonRole = Role::find(3);
        if (empty($bitonRole)) {
            $bitonRole = Role::create(['id' => 3, 'name' => 'Pengelola', 'slug' => 'pengelola', 'description' => 'Pengelola SI BITON', 'level' => 3]);
            $bitonRole->id = 3;
            $bitonRole->save();
        } else {
            $bitonRole->update(['id' => 3, 'name' => 'Pengelola', 'slug' => 'pengelola', 'description' => 'Pengelola SI BITON', 'level' => 3]);
            $bitonRole->save();
        }

        $peroranganRole = Role::find(4);
        if (empty($peroranganRole)) {
            $peroranganRole = Role::create(['id' => 4, 'name' => 'Perorangan', 'slug' => 'perorangan', 'description' => 'Perorangan', 'level' => 3]);
            $peroranganRole->id = 4;
            $peroranganRole->save();
        } else {
            $peroranganRole->update(['id' => 4, 'name' => 'Perorangan', 'slug' => 'perorangan', 'description' => 'Perorangan', 'level' => 3]);
            $peroranganRole->save();
        }
        

        $badanHukumRole = Role::find(5);
        if (empty($badanHukumRole)) {
            $badanHukumRole = Role::create(['id' => 5, 'name' => 'Badan Hukum', 'slug' => 'badan-hukum', 'description' => 'Badan Hukum', 'level' => 3]);
            $badanHukumRole->id = 5;
            $badanHukumRole->save();
        } else {
            $badanHukumRole->update(['id' => 5, 'name' => 'Badan Hukum', 'slug' => 'badan-hukum', 'description' => 'Badan Hukum', 'level' => 3]);
            $badanHukumRole->save();
        }
        
        /*
         ** Attach role
        */
        $user = User::find(0);
        $user->attachRole($rootRole);
        $user = User::find(1);
        $user->attachRole($superAdminRole);
        $user = User::find(2);
        $user->attachRole($adminRole);
    }
}

class PermissionSeeder extends Seeder
{
    
    public function run() {
        Model::unguard();
        DB::statement("SET foreign_key_checks = 0");
        DB::table('permissions')->truncate();
        
        $permissions = [];
        
        $routeCollection = Route::getRoutes();
        
        /*
         ** All route permission
        */
        foreach ($routeCollection as $value) {
            $action = $value->getAction();
            
            if (isset($action['prefix']) && $action['prefix'] = 'v1/' && isset($action['as'])) {
                $route = explode(".", $action['as'], 2);
                $route = $route[1];

                // Jangan Buat Permission yang tidak digunakan
                if(strpos($route, 'create') || strpos($route, 'edit')) continue;
                
                $route_name = ucwords(preg_replace('/\./', ' ', $route));
                
                $route_group = explode(" ", $route_name);
                $route_group = $route_group[0];
                
                $permissions[] = ['name' => $route_name, 'name_group' => $route_group, 'slug' => $route, 'slug_view' => 'app.' . $route, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ];
            }
        }
        
        Permission::insert($permissions);
    }
}

class PermissionRoleSeeder extends Seeder
{
    public function run() {
        Model::unguard();
        // DB::statement("SET foreign_key_checks = 0");
        
        /**
         * Role Root
         */        
        $permission_role = [];
        $permissions = Permission::get();
        foreach ($permissions as $key => $value) {
            $permission_role[] = $value->id;
        }
        $role = Role::find(0);
        $role->permissions()->sync($permission_role);

        /**
         * Role Super Admin
         */        
        $permission_role = [];
        $permissions = Permission::whereNotIn('name_group', ['Permission'])->get();
        foreach ($permissions as $key => $value) {
            $permission_role[] = $value->id;
        }
        $role = Role::find(1);
        $role->permissions()->sync($permission_role);

        /**
         * Role Admin
         */        
        $permission_role = [];
        $permissions = Permission::whereNotIn('name_group', ['Permissions', 'Roles'])->get();
        foreach ($permissions as $key => $value) {
            $permission_role[] = $value->id;
        }
        $role = Role::find(2);
        $role->permissions()->sync($permission_role);

        /**
         * Role User
         */        
        $permission_role = [];
        $permissions = Permission::whereIn('name_group', ['Seeds', 'Plantings'])->get();
        foreach ($permissions as $key => $value) {
            $permission_role[] = $value->id;
        }
        $role = Role::find(3);
        $role->permissions()->sync($permission_role);
        $role = Role::find(4);
        $role->permissions()->sync($permission_role);
        $role = Role::find(5);
        $role->permissions()->sync($permission_role);
    }
}

class OauthClientSeeder extends Seeder
{
    
    public function run() {
        Model::unguard();
        $data = array(['id' => '1', 'secret' => 'mIeCPklWs4MbbzmA2Mel', 'name' => 'Administrator', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]);
        
        $exist = DB::table('oauth_clients')->where('id', '1')->count();
        
        if (!$exist) {
            DB::table('oauth_clients')->insert($data);
        }
    }
}

/** MASTER DATA SEEDER : Contoh untuk tabel district */

// ./artisan db:seed --class LocationTableSeeder
class LocationTableSeeder extends Seeder
{
    public function run() {
        Model::unguard();
        
        DB::statement("SET foreign_key_checks = 0");
        DB::table('locations')->truncate();
        foreach (['provinces.csv', 'cities.csv', 'districts.csv', 'village1.csv', 'village2.csv', 'village3.csv', 'village4.csv', 'village5.csv' ] as $fileName) {
            echo ">> Opening file $fileName \n";
            $sheetCollection = \Excel::load('resources/'.$fileName, function ($reader) {
            })->get();

            if (!empty($sheetCollection) && count($sheetCollection)) {
                // DB::beginTransaction();
                try {
                    foreach ($sheetCollection as $key => $sheet) {
                        echo "Open sheet $key \n";
                        $dataset = [];
                        for ($i=0, $l = count($sheet); $i < $l; $i++) {
                            $cellCollection = $sheet[$i];
                            $dataset[] = [
                                'province_id' => (string) $cellCollection->get('province_id'),
                                'city_id' => (string) $cellCollection->get('city_id'),
                                'district_id' => (string) $cellCollection->get('district_id'),
                                'village_id' => (string) $cellCollection->get('village_id'),
                                'name' => (string) $cellCollection->get('name'),
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                            ];
                        }
                        do {
                            $proccess = array_splice($dataset, 0, 5000);
                            echo "Insert ".count($proccess)." rows \n";
                            DB::table('locations')->insert($proccess);
                        } while (count($dataset) > 0);
                        echo "Total Record ".DB::table('locations')->count()." rows \n";
                    }
                }
                catch(Exception $e) {
                    // \DB::rollback();
                }
                // \DB::commit();
            }
        }
    }
}


/** MASTER DATA SEEDER : Contoh untuk tabel district */
class DistrictTableSeeder extends Seeder
{
    public function run() {
        Model::unguard();
        // Getting all results
        $sheetCollection = \Excel::load('resources/KECAMATAN.xls', function ($reader) {
        })->get();

        if (!empty($sheetCollection) && count($sheetCollection)) {
            DB::statement("SET foreign_key_checks = 0");
            DB::beginTransaction();
            DB::table('districts')->truncate();
            try {
                foreach ($sheetCollection as $key => $sheet) {
                    
                    $dataset = [];
                    foreach ($sheet as $row => $cellCollection) {
                        $dataset[] = [
                            'code' => $cellCollection->get('id_kec'),
                            'name' => $cellCollection->get('kecamatan'),
                            'city_id' => 32,
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                        ];
                    }
                    DB::table('districts')->insert($dataset);
                }
            }
            catch(Exception $e) {
                \DB::rollback();
            }
            \DB::commit();
        }
    }
}

class DistrictUserAdminSeeder extends Seeder
{
    public function run() {
        Model::unguard();
        // User Root dan Super Admin mempunyai akses ke semua kecamatan
        $districts = \App\Models\District::select(['id',])->get();
        $users = User::whereIn('id', [0, 1, 2])->get();
        foreach ($users as $key => $user) {
          $user->districts()->sync($districts);
        }
    }
}

/** Please Run this seeder manually: `phh artisan db:seed --class DefaultDistrictUserSeeder` */
class DefaultDistrictUserSeeder extends Seeder
{
    public function run() {
        Model::unguard();
        // User Root dan Super Admin mempunyai akses ke semua kecamatan
        $districts = \App\Models\District::select(['id', 'name'])->get();
        // $users = User::whereIn('id', [0, 1, 2])->get();
        $role = Role::find(3);
        foreach ($districts as $key => $district) {
            $name = 'Admin '.$district->name;
            $parsedName = strtolower(str_replace(' ', '-', $name));
            if(!User::where('name', 'like', '%'.$name.'%')->count()) {
                $user = new User;
                $user->name = $name;
                $user->password = Hash::make($parsedName);
                $user->email = strtolower($parsedName.'@tasik.go.id');
                $user->save();
                $user->attachRole($role);
                $district->users()->attach($user);
            }
        }
    }
}

