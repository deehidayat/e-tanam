1. Untuk membuat CRUD lakukan langkah seperti berikut
API
- Buat Migrasi `php artisan make:migration create_namatabels_table` (Catatan: namatabel ditambah huruf s atau es)
- Buat Model `php artisan make:model Models\Namatabel` (Catatan: Namatabel tanpa huruf s)
- Copy contoh Repository dari file `cp app\Libraries\Repositories\DistrictRepository.php app\Libraries\Repositories\NamatabelRepository.php` (Catatan: Namatabel tanpa huruf s)
- Copy contoh Controller dari file `cp app\Http\Controllers\API\DistrictAPIController.php app\Http\Controllers\API\NamatabelAPIController.php` (Catatan: Namatabel tanpa huruf s)
- Tambahkan Route di file app\Http\routes.php <= Route::resource('v1/namatabels', 'API\NamatabelAPIController'); (Catatan: namatabel ditambah huruf s/es)

Angular
- Copy file `cp public\scripts\controllers\districts.js public\scripts\controllers\namatabels.js` (Catatan: namatabel ditambah huruf s/es)
- Copy folder view `cp -r public\views\districts public\views\namatabels` (Catatan: namatabel ditambah huruf s/es)
- Tambahkan di public/index.html paling bawah <script src="scripts/controllers/namatabels.js"></script> 
- Tambahkan link di public\views\app.html di bagian element UL dengan id navigation (ul#navigation)


